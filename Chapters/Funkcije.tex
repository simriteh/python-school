% Chapter 5

\chapter{Funkcije} % Chapter title
\label{ch:funkcije} % For referencing the chapter elsewhere, use \autoref{ch:mathtest}
\index{funkcije}

Funkcije su grupirani i izolirani dio koda koji izvodi određeni zadatak. Prednosti upotrebe funkcija su:
\begin{itemize}
 \item Smanjenje ponavljanja istog koda
 \item Povećavanje modularnosti koda - raščlanjivanje složenog koda na jednostavnije djelove
 \item Povećanje čitkosti koda.
\end{itemize}

Rad sa funkcijama u Pythonu omogućuje veliku fleksibilnost. Funkcije mogu biti dodijeljene varijablama, spremljene u listama i prosljeđivane kao argumenti drugim funkcijama.

Razlikujemo dva osnovna tipa Python funkcija:
\begin{itemize}
 \item Ugrađene funkcije su funkcije koje su dio Python programskog jezika
 \item Funkcije definirane od strane korisnika.
\end{itemize}

%----------------------------------------------------------------------------------------
\section{Definiranje i pozivanje funkcija}
\label{sec:definiranje_i_pozivanje_funkcija}
\index{def}

Ključna riječ \texttt{def} omogućuje definiranje funkije. Najjednostavnija sintaksa koja omogućuje definiranje funkcije je:
\begin{lstlisting}[language=Python, style=Syntax, morekeywords={}]
def functionname():
    commands
\end{lstlisting}
gdje je \texttt{functionname} odabrano ime funkcije, a \texttt{commands} je naredba ili više naredbi koje funkcija izvršava.
\marginpar{Poziv funkcije uvijek mora biti nakon definicije funkcije. Ne može se pozvati funkcija koja prethodno nije definirana.}
Prethodno definiranu funkciju možemo izvršiti pozivom te funkcije. Funkcija se poziva pomoću imena funkcije nakon kojeg slijede zagrade. 

\lstinputlisting[language=Python, style=File, caption={Definicija i pozivanje jednostavne funkcije}]{gfx/Funkcije/funkcije01.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
Pozdrav!
\end{lstlisting}


Funkciju je moguće proširiti na način  da prima jedan ili više argumenata. Argumenti se definiraju pomoću imena argumenata razdvojenih zarezom u zagradama nakon imena funkcije:
\begin{lstlisting}[language=Python, style=Syntax, morekeywords={}]
def functionname(arg1, arg2, ... , argn):
    commands
\end{lstlisting}

Funkcija sa argumentima se poziva tako da se u zagradama nakon imena funkcije upišu vrijednosti argumenata. Argumenti se u pozivu funkcije razdvajaju zarezom kao i prilikom definicije funkcije.
\lstinputlisting[language=Python, style=File, caption={Definicija funkcije koja prima argument}]{gfx/Funkcije/funkcije02.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
Pozdrav Marko !
2 + 5 = 7
\end{lstlisting}


%----------------------------------------------------------------------------------------
\section{Vraćanje vrijednosti iz funkcije}
\label{sec:vracanje_vrijednosti_iz_funkcije}
\index{return}

Funkcija može vratiti vrijednost nakon poziva funkcije. Vrijednost koju funkcija vraća doslovno zamjenjuje funkciju u kontekstu u kojem je ona pozvana, a tip vraćenog podatka može biti bilo koji Python objekt. Vraćanje vrijednosti se definira naredbom \texttt{return} nakon koje se zaustavlja izvršavanje funkcije:
\begin{lstlisting}[language=Python, style=Syntax, morekeywords={return, def}]
def functionname(arg1, arg2, ... , argn):
    commands
    return value
\end{lstlisting}

\lstinputlisting[language=Python, style=File, caption={Definicija funkcije koja prima više argumenata i vraća vrijednost}]{gfx/Funkcije/funkcije03.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
8 3
\end{lstlisting}

Funkcija može sadržavati više \texttt{return} naredbi (obično u različitim uvjetnim grananjima) i svaka može vratiti različit tip podatka (Izvorni kod \ref{lst:funkcije04}).

\lstinputlisting[language=Python, style=File, caption={Funkcija koja vraća različite tipove}, label=lst:funkcije04]{gfx/Funkcije/funkcije04.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
8 malo teksta
\end{lstlisting}

Vrlo često, javlja se potreba da funkcija vraća više vrijednosti. To se može ostvariti vraćanjem tuplea.
\lstinputlisting[language=Python, style=File, caption={Funkcija koja vraća više vrijednosti korsiteći tuple}, label=lst:funkcije_return_tuple]{gfx/Funkcije/funkcije_return_tuple.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
(8.0, 4.0)
8.0
4.0
\end{lstlisting}
%----------------------------------------------------------------------------------------
\section{Argumenti sa zadanim vrijednostima}
\label{sec:argumenti_sa_zadanim_vrijednostima}

Funkcije mogu biti definirane i sa zadanim standardnim vrijednostima argumenata. Argumenti sa zadanim vrijednostima moraju biti definirani nakon argumenata za koje nije postavljena standardna vrijednost.
\begin{lstlisting}[language=Python, style=Syntax]
def functionname(arg1=val1, arg2=val2, ... , argn=valn):
    commands
    return value
\end{lstlisting}

Prilikom poziva funkcije, moguće je funkciju pozvati tako da se zadaju samo oni argumenti koji nemaju standardnu vrijednost.   

\lstinputlisting[language=Python, style=File, caption={Funkcija sa zadanim argumentom}, label=lst:funkcije05]{gfx/Funkcije/funkcije05.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
4.0 20.0 0.0
\end{lstlisting}

\section{Keyword i non-keyword argumenti}
\label{sec:keyword_i_nonkeyword_argumenti}

Funkciju se može pozivati na način da se, pomoću keyword argumenata, točno specificira vrijednost argumenta preko njihovog naziva. Keyword argumenti su oni argumenti koji se, pri pozivu funkcije, zadaju imenom i vrijednošću, za razliku od non-keyword argumenata koji se zadaju samo kao vrijednost. Redosljed zadavanja keyword argumenata može biti proizvoljan dok kod kombinacije non-keyword i keyword argumenata prvo moraju biti specificirani non-keyword argumenti.
\begin{lstlisting}[language=Python, style=File]
# Definiranje funkcije
def f(x, a, b):
    return x*(a+b)

# Poziv funkcije sa keyword argumentima
f(x=10, b=0, a=8)

# Poziv funkcije sa mjesovitim keyword i non-keyword argumentima
f(10, b=0, a=8)
\end{lstlisting}

Posebna sintaksa definiranja funkcije pomoću \texttt{*} i \texttt{**} omogućuje korištenje varijabilnog broja argumenata funkcije. Argument definiran sa jednom zvjezdicom, uobičajeno \texttt{*args}, obuhvaća sve non-keyword argumente u jedan tuple. Upotrebom dvostruke zvijezdice \texttt{**kwargs} omogućuje pohranu svih keyword argumenata u dictionary gdje su ključevi imena argumenata a vrijednosti argumenata su pohranjene u vrijednosti pod pripadajućim ključem.

\lstinputlisting[language=Python,
style=File,
caption={Funkcija sa \texttt{*args} i \texttt{**kwargs} argumentima},
label=lst:funkcije_args_kwargs_01]
{gfx/Funkcije/funkcije_args_kwargs_01.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
non-keyword argumenti: (10, 'bla bla')
keyword argumenti: {'a': 3, 'c': (0, 0), 'b': True}
a = 3
c = (0, 0)
b = True
\end{lstlisting}

\marginpar{Prilikom korištenja oba operatora za ``raspakiravanje'' argumenata (\texttt{*} i \texttt{**}) važno je poštivati redoslijed. I kod definicije funkcije i kod poziva funkcije prvo se koriste non-keyword argumenti a ond akeyword argumenti.}
Osim prilikom definicije funkcije, sintaksa \texttt{*args} i \texttt{**kwargs} može se koristiti i pri pozivu funkcije. Zvijezdicom \texttt{*} se tuple vrijednosti ``raspakiraju'' u argumente funkcije. Analogno, keyword argumente definirane u dictionaryu može se proslijediti funkciji pomoću dvije zvijezdice \texttt{**}. U izvornom kodu \ref{lst:funkcije_args_kwargs_02} prikazano je korištenje \texttt{*args} i \texttt{**kwargs} prilikom poziva funkcije.

\lstinputlisting[language=Python,
style=File,
caption={\texttt{*args} i \texttt{**kwargs} argumenti prilikom poziva funkcije},
label=lst:funkcije_args_kwargs_02]
{gfx/Funkcije/funkcije_args_kwargs_02.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
argumenti:
 a: 1
 b: 2
 c: 3
 d: 4
 e: 5
argumenti:
 a: 3
 b: 5
 c: 2
 d: 1
 e: 4
argumenti:
 a: 5
 b: 4
 c: 3
 d: 2
 e: 1
\end{lstlisting}
%----------------------------------------------------------------------------------------
\section{Ugnježdena definicija funkcije}
\index{ugnježdena definicija funkcije}
\index{closure}

Ugnježdeno definiranje funkcije (eng. \emph{closure} ili \emph{nested function definition}) omogućuje definiciju funkcije u funkciji. Ugnježdena definicija funkcije je moguća u Pythonu jer su funkcije objekti te ih je moguće prosljeđivati kao argumente drugim funkcijama te vraćati kao rezultat funkcije. Ugnježdeno definirane funkcije mogu se pokazati korisne jer unutarnja funkcija može koristiti objekte definirane u vanjskoj:

\begin{lstlisting}[language=Python, style=Terminal]
>>> def vanjska(vanjski_argument):
...     def unutarnja(unutarnji_argument):
...         return vanjski_argument * unutarnji_argument
...     return unutarnja
...
>>> f = vanjska(5)
>>> f(3)
15
>>> f(4)
20
\end{lstlisting}

%----------------------------------------------------------------------------------------
\section{Anonimne funkcije}
\index{lambda}
\index{lambda funkcije}
\index{anonimne funkcije}

Lambda funkcije su funkcije bez imena tj. anonimne funkcije. Najčešće se koriste za definiranje vrlo jednostavnih i kratkih funkcija. Definiranje anonimne funkcije se vrši korištenjem ključne riječi \texttt{lambda}:
\begin{lstlisting}[language=Python, style=Syntax]
 lambda argumenti: izraz
\end{lstlisting}


Upotrebu jednostavne funkcije koja vraća apsolutnu razliku dva broja
\begin{lstlisting}[language=Python, style=File]
def diff(a, b):
    return abs(a - b)

print diff(7, 11)
\end{lstlisting}
\begin{lstlisting}[language=Terminal, style=Terminal]
4
\end{lstlisting}
može se zamjeniti lambda funkcijom:
\begin{lstlisting}[language=Python, style=File]
>>> print (lambda a, b: abs(a - b))(7, 11)
4
\end{lstlisting}

Anonimne funkcije se često pokriste kao argumenti drugih funkcija koje očekuju objekt tipa funkcija kao argument.

\texttt{sorted} funkcija očekuje argument \texttt{key} kao funkciju, koja je većini slučajeva veoma jednostavna funkcija i obično se koristi anonimna funkcija da se izbjegne posebno definiranje funkcije. U sljedećem primjeru napravljeno je sortiranje liste tupleova po drugim članovima tupleova:
\begin{lstlisting}[language=Python, style=Terminal]
>>> sorted([(5, 5), (13, 2), (11, 6), (7, 4)], key=lambda a: a[1])
[(13, 2), (7, 4), (5, 5), (11, 6)]
\end{lstlisting}
U sljedećem primjeru prikazano je filtriranje članova niza djeljivih sa 7:
\begin{lstlisting}[language=Python, style=Terminal]
>>> lista=range(2,100,2)
>>> print filter(lambda x:x%7==0, lista)
[14, 28, 42, 56, 70, 84, 98]
\end{lstlisting}

Još jedan primjer upotrebe anonimnih funkcija je u ugnježdenim definicijama funkcijama.


\lstinputlisting[language=Python,
                 style=File,
                 caption={Primjer anonimne funkcije u ugnježdenoj definiciji},
                 label=lst:lambda_closure]
                 {gfx/Funkcije/lambda_closure.py}
                 
\begin{lstlisting}[language=Terminal, style=Terminal]
Mario: Bok!
Mario: Kako ste?
\end{lstlisting}

\section{Rekurzivne funkcije}
\index{rekurzivne funkcije}
\index{rekurzija}

Do sada smo se već susreli sa petljama i načinom kako funkcionira iterativni postupak kao sukcesivno povećavanje indeksa, tj. brojčanika koji sudjeluje u operaciji koja u konačnici daje finalni rezultat.
Primjer je suma uzastopnih brojeva od 1 do 100.

\begin{lstlisting}[language=Python, style=File]
suma = 0
for index in range(1,101):
  suma = suma + index

print suma
\end{lstlisting}

Znači suma se postepeno povećava dok se ne iscrpi lista, tj. sekvenca indeksa definirana pozivom na \textit{range} naredbu.

\begin{lstlisting}[language=Python, style=Syntax]
  index = 1
      suma = 0 + 1
  index = 2
      suma = 1 + 2
  index = 3
      suma = 3 + 3
  index = 4
      suma = 6 + 4
\end{lstlisting}
i tako dalje dok \textit{index} ne postane 100. Drugim riječima postepeno se na varijablu nadodaju brojevi.

Takoder je moguće zapisati isti postupak prema koracima kako je vršeno sumiranje

\begin{lstlisting}[language=Python, style=Syntax]
suma = (0)
index = 1 : suma = ((0) + 1)
index = 2 : suma = (((0) + 1) + 2)
index = 3 : suma = ((((0) + 1) + 2) + 3)
itd.
\end{lstlisting}

Kada bi indeksirali parcijalne sume tada bi dobili slijedeći izraz $$suma_{index} = suma_{index-1} + index$$.
Tako zapisani izraz daje naslutiti kako je slijedeća suma zapisana kao suma iz prethodnog koraka na koju se nadodaje vrijednost index iz trenutnog koraka iterativne petlje.
Ovakav zapis u biti je obrnuti zapis od prirodnog sukcesivnog povećavanja indeksa:

\begin{lstlisting}[language=Python, style=Syntax]
suma(100) = suma(99) + 100, gdje je
suma(99) = suma(98) + 99, gdje je
suma(98) = suma(97) + 98, i tako dalje sve do
.
.
.
suma(1) = suma(0) + 1
\end{lstlisting}
\marginpar{Mogućnost zapisa funkcije u obliku koji poziva istu funkciju nazivamo \textbf{rekurzija}, a takvu funkciju \textbf{rekurzivna funkcija}.}

\textbf{Tipična rekurzivna funkcija vrši poziv sama na sebe}. 

Primjer je funkcija \textit{rsuma} :
\begin{lstlisting}[language=Python, style=File]
def rsuma(x):
  return rsuma(x)
\end{lstlisting}
No, kao što vidite ova funkcija vraća poziv na samu sebe koja vraća poziv na samu sebe koja vraća poziv na samu sebe i tako u beskonačnost. 
Ovako definirana rekurzivna funkcija nema kraja, a program ulazi u \textit{beskonačnu petlju} (eng. \textit{infinite loop}). A gdje je greška?
Kako bi spriječili ovakvo ponašanje potrebno je uvesti \textit{uvjet prekida}. To je granična vrijednost nakon koje funkcija više neće vratiti sebe već neku vrijednost.
Ako se sada vratimo na sumu brojeva onda bi rekurzivna funkcija sume trebala izgledati ovako:

\begin{lstlisting}[language=Python, style=File]
def rsuma(n):
  return rsuma(n - 1) + n
\end{lstlisting}

No oznaka prekida je \textbf{suma(0)=0}, tj. kada u 100-tom pozivu funkcije \textbf{rsuma} bude primila za argument 0 potrebno je zaustaviti daljnje pozive \textbf{rsuma} funkcije.
To ćemo postići testiranjem vrijednosti \textit{n}

\begin{lstlisting}[language=Python, style=File]
def rsuma(n):
  if n = 0:
    return 0
  else:
    return rsuma(n - 1) + n
\end{lstlisting}

Na taj način omogućili smo oznaku kraja rekurzivnim pozivima funkcije.
\marginpar{\textit{Rekurzivni zapis unosi konciznost i eleganciju u programski kod.}}
Slijedi nekoliko primjera:

\textbf{Primjer 1.} Proračun faktorijela broja n.

Faktorijel broja n je jednak :

$$n! = 1*2*3*4*...*n$$

Ima rekurzivni zapis $$n! = (n-1)! * n$$

\begin{lstlisting}[language=Python, style=File]
def factoriel(n):
  if n = 0:
    return 1
  else:
    return factoriel(n - 1) * n
\end{lstlisting}

\textbf{Primjer 2.} Potenciranje broja \textit{x} eksponentom \textit{n}.

Potenciranje također je iterativni proces koji može biti zapisan rekurzivno na slijedeći način

$$x^{n} = x^{n-1} * x$$ 

Primjerice kub je jednak umnošku broja i kvadrata broja,

$$x^{3} = x^{2} * x$$

\begin{lstlisting}[language=Python, style=File]
def potencija(x,n):
  if n = 0:
    return 1
  else:
    return potencija(x, n - 1) * x
\end{lstlisting}

\textbf{Primjer 3.} Korisnički unos i formiranje liste

Unose se brojevi koji se dodaju na listu dok se ne unese \texttt{'0'}.

\begin{lstlisting}[language=Python, style=File]
def rloop():
	  x = int(raw_input('Unesi broj ili 0 za kraj'))

	  if x:
		  return [x]+rloop()
	  else:
		  return []
\end{lstlisting}
Dok su prethodni primjeri ovisili o smanjivanju broja n do oznake kraja, u ovom primjeru funkcija se poziva tako da ažurira listu novim vrijednostima.

Za zaključak, potrebno je napomenuti da je rekurzivnu funkciju moguće napraviti za svaki poziv petlje, no da li je nužno sve petlje pretvoriti u rekurzivne funkcije To ostaje na procijeni programeru koji mora odvagnuti količinu elegancije, hijerarhije, strukturiranosti te preglednosti koda.

