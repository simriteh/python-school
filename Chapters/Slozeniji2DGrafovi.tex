
\chapter{Složeniji 2D grafovi} % Chapter title
\label{ch:slozeniji_2d_grafovi} % For referencing the chapter elsewhere, use \autoref{ch:mathtest}

\section{Izolinije}
\index{izolinije}
\index{contour}
\index{contourf}

Prilikom vizualizacije ovisnosti funkcije (ili podataka) o dvije varijable (2D) često se koriste izolinije.
Izolinije su krivulje koje povezuju točke iste (skalarne) vrijednosti.

Izolinija je skup rješenja jednadžbe
\begin{equation}
 f(x,y) = c_i
 \label{eq:isolinija}
\end{equation}
gdje je $c_i$ konstanta za koju se određuje izolinija. Ako za dani $c_i$ postoji rješenje jednadžbe \eqref{eq:isolinija}, to rješenje predstavlja jednu ili više krivulja u ravnini $x-y$.
Za više parametara $\mathbf{c} = (c_1, c_2, ..., c_n)$ dobiva se familija krivulja koje služe za 2D vizualizaciju funkcija dvije varijable.

Matplotlib nudi mogućnost naprednog uređivanja i kontrolabilnost izolinijskih grafova. 
Funkcija \texttt{contour} služi za crtanje izolinija a \texttt{countourf} za bojanje površina između izolinija. 

\begin{lstlisting}[language=Python, style=Syntax, keywords={contour, contourf}]
matplotlib.pyplot.contour
matplotlib.pyplot.contourf
\end{lstlisting}

Obje navedene funkcije imaju identičnu osnovnu sintaksu, a razlikuju se u svega nekoliko argumenta koji su specifični za stiliziranje linija odnsno bojanje ploha.

U najjednostavnijoj varijanti, funkcije primaju jedan argumenti i to matricu \texttt{Z} na temelju čijih vrijednosti se crtaju izolinije i plohe. Razmaci između redaka i stupaca su jednaki 1 a broj izolinija se određuje automatski.
\begin{lstlisting}[language=Python, style=Syntax, keywords={contour, contourf}]
matplotlib.pyplot.contour(Z)
matplotlib.pyplot.contourf(Z)
\end{lstlisting}

Broj nivo linija se može specificirati cijelim brojem \texttt{n} ili se mogu odrediti željene vrijednosti pomoću vektora \texttt{V} za koje će se crtati izolinije.
\begin{lstlisting}[language=Python, style=Syntax, keywords={contour, contourf}]
matplotlib.pyplot.contour(Z, n)
matplotlib.pyplot.contourf(Z, n)
matplotlib.pyplot.contour(Z, V)
matplotlib.pyplot.contourf(Z, V)
\end{lstlisting}

Razmaci između stupaca i redaka mogu se zadati pomoću matrica \texttt{X} i \texttt{Y}:
\begin{lstlisting}[language=Python, style=Syntax, keywords={contour, contourf}]
matplotlib.pyplot.contour(X, Y, Z)
matplotlib.pyplot.contour(X, Y, Z, n)
matplotlib.pyplot.contourf(X, Y, Z, n)
matplotlib.pyplot.contour(X, Y, Z, V)
matplotlib.pyplot.contourf(X, Y, Z, V)
\end{lstlisting}
Matrice \texttt{X} i \texttt{Y} sadrže $x$ i $y$ kordinate pripadajućih elemenata u matrici \texttt{Z} te sve tri matrice moraju biti istog oblika/dimenzija. Za jednostavljeno kreiranje takvih matrica na temelju vektora koordinata koristi se funkcija \texttt{meshgrid}\index{meshgrid} (vidi poglavlje~\ref{sec:meshgrid}). 


\subsection{Mapa boja}
\label{subsec:mapa_boja}
\index{mapa boja}
\index{colormap}

Mapa boja (eng. \emph{colormap}) je način na koji se numerička vrijednost sa intervala $[0,1]$ pretvara u RGBA (\emph{Red}, \emph{Green}, \emph{Blue}, \emph{Alpha}) specifikaciju boje kojom se definira udio crvene, zelene i plave boje te prozirnost.

Mapiranje boja se odvija u dva koraka: podaci koje treba vizualizirati su prvo mapirani na interval $[0,1]$ koristeći klasu \texttt{matplotlib.colors.Normalize} ili neku od njenih podklasa; onda se broj sa intervala $[0,1]$ mapira u određenu boju koristeći mapu boja.

Matplotlib sadrži unaprijed definirane mape boja koje se nalaze u \emph{matplotlib.pyplot.cm} modulu (Slika~\ref{fig:colormaps}).

\begin{figure}[H]
 \centering
 \includegraphics[width=\textwidth]{gfx/Slozeniji2DGrafovi/colormaps.pdf}
 \caption{Predefinirane mape boja u matplotlibu}
 \label{fig:colormaps}
\end{figure}

Mapa boja se postavlja pomoću \emph{keyword} argumenta \texttt{cmap}:
\begin{lstlisting}[language=Python, style=Syntax, keywords={contour, contourf}]
matplotlib.pyplot.contour(X, Y, Z, cmap=matplotlib.pyplot.cm.hot)
\end{lstlisting}

Osim predefiniranih mapa boja, mogu se koristiti i ručno definirane mape boja.

\subsection{Detaljnije podešavanje grafa izolinija}

Podešavanje boja u grafovima izolinija moguće je se \emph{keyword} argumentom \texttt{colors} u \texttt{contour} i \texttt{contourf} funkcijama. U funkciji \texttt{contour} boje se apliciraju na izolinije a u funkciji \texttt{contourf} boje se apliciraju na površine između izolinija.

Tri su osnovna načina zadavanja boje:
\begin{itemize}
 \item \texttt{colors = None}, boje se preuzimaju iz postavljene mape boja (vidi \ref{subsec:mapa_boja})
 \item \texttt{colors = 'r'}, postavlja se jedna boja za sve izolinije ili površine između izolinija. 
 \item \texttt{colors = ['r', 'g', 'b']}, postavlja se lista (ili tuple) boja. Boje iz liste se ponavljaju, ako ima više izolinija nego specificiranih boja.
\end{itemize}

Pojedinu boju ili boju u listi se može zadavati na više načina, kao što je opisano u poglavlju (\ref{subsec:boje}).

Kada u istom prostoru crtanja postoji više grafova, bolja vidljivost svih grafova se postiže prozirnošću. Prozirnost izolinija ili površina između njih postavlja se pomoću \emph{keyword} argumenta \texttt{alpha}\index{alpha}\index{prozirnost}. \texttt{alpha} može primiti decimalni broj sa intervala $[0,1]$ gdje je $0$ potpuno prozirno (nevidljivo) a $1$ je potpuno neprozirna boja. 


\subsection{Primjer}

\lstinputlisting[language=Python, 
                 style=File,
                 caption={Primjer vizualizacije izolinija},
                 label=lst:contour_01]
                 {gfx/Slozeniji2DGrafovi/contour_01.py}
                 
\begin{figure}[H]
 \centering
 \includegraphics[width=\textwidth]{gfx/Slozeniji2DGrafovi/contour_01.pdf}
 \caption{Graf napravljen izvođenjem izvornog koda \ref{lst:contour_01}}
 \label{fig:contour_01}
\end{figure}