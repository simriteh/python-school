
\chapter{Numeričko integriranje}
\label{ch:numericko_integriranje}
\index{integriranje}

Numeričkim integriranjem rješava se određeni integral funkcije $f(x)$ na intervalu $[a,b]$
\begin{equation}
 I=\int_a^b f(x) dx
 \label{eq:odredjeni_integral_1}
\end{equation}


\section{Trapezna formula}
\index{trapezna formula}

Najjednostavniji način za aproksimiranje određenog integrala je izračun površine ispod najjednostavnije aproksimacijske krivulje - pravca.
Pravac je definiran početnom i krajnjom točkom intervala inegrala te zatvara površinu sa x-osi oblika trapeza.
Izračun površine ispod pravca temelji se na trapeznoj formuli tj. izračunu površine trapeza:
\begin{equation}
  \int_{a}^{b} f(x)\, dx \approx (b-a) \left[\frac{f(a) + f(b)}{2} \right].
  \label{eq:trapezna_formula1}
\end{equation}

Svojstvo određenog integrala je da ga možemo rastaviti na konačni broj djelova, te je tada određeni integral nad početnim intervalom jednak zbroju integrala na djelovima tog integrala:
\begin{equation}
 \displaystyle \int\limits _a^b f(x)  dx=\int\limits _a^c f(x)  dx+\int\limits _c^b f(x) dx .
 \label{eq:odredjeni_integral_2}
\end{equation}

Uzevši u obzir rastavljanje određenog integrala \eqref{eq:odredjeni_integral_2}, trapeznu formulu \eqref{eq:trapezna_formula1} može se koristiti po djelovima intervala $[a,b]$:
\begin{equation}
  \int_{a}^{b} f(x)\, dx \approx \frac{1}{2} \sum_{k=1}^{N} \left( x_{k+1} - x_{k} \right) \left( f(x_{k+1}) + f(x_{k}) \right)
  \label{eq:trapezna_formula_neuniformno}
\end{equation}
gdje je $N$ broj podintervala za koje vrijedi $a < x_1 < x_2 < \cdots < x_{N-1} < x_N < b$ .

\begin{figure}[h!]
 \centering
 \includegraphics[width=\textwidth]{gfx/NumerickoIntegriranje/trapezna_formula_skica.pdf}
 % kate.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
 \caption{Trapezi na cjelom intervalu $[a,b]$ i na tri podintervala.}
 \label{fig:integr_trapezna_skica}
\end{figure}

Za unifomnu podjelu intervala $[a,b]$, gdje su svi podintervali jednako široki, trapezna formula \eqref{eq:trapezna_formula_neuniformno} se može pojednostaviti:
\begin{equation}
  \int_{a}^{b} f(x)\, dx \approx \frac{h}{2} \sum_{k=1}^{N} \left( f(x_{k+1}) + f(x_{k}) \right)
  \label{eq:trapezna_formula_uniformno}
\end{equation}
gdje je $h$ širina podintervala.


Modul \texttt{numpy.integrate} sadrži funkciju \texttt{trapz}\index{trapz} za numeričku integraciju pomoću trapezne formule.
\begin{lstlisting}[language=Python, style=Syntax, keywords={trapz}]
scipy.integrate.trapz(y, x=None, dx=1.0)
\end{lstlisting}
Funkcija prima set (vektor) vrijednosti podintegralne funkcije u točkama diskretiziranog intervala $[a,b]$. Za neuniformnu diskretizaciju intervala $[a,b]$ potrebno je funkciji proslijediti vrijednosti diskretizirane varijable integracije (vektor x-eva) a za uniformnu diskretizaciju dovoljno je specificirati širinu podintervala. Ukoliko je funkciji \texttt{trapz} proslijeđen samo vektor y, pretpostavlja se da je riječ o uniformnoj diskretizaciji sa širinom podintervala 1.

U izvornom kodu \ref{lst:integr_trapezna} rješen je određen integral na uniformno diskretiziranom intervalu implementacijom trapezne formule \eqref{eq:trapezna_formula_uniformno} te korištenjem funkcije \texttt{trapz} sa vektorom diskretizirane varijable integracije i sa širinom podintervala.
\lstinputlisting[language=Python,
         style=File,
         caption={Primjer računanja određenog inegrala pomoću trapezne formule},
         label=lst:integr_trapezna]
         {gfx/NumerickoIntegriranje/trapezna.py}
         
\begin{lstlisting}[language=Terminal, style=Terminal]
Rjesenje 1: 50.419500
Rjesenje 2: 50.419500
Rjesenje 3: 50.419500
\end{lstlisting}


\begin{figure}[h!]
 \centering
 \includegraphics[width=\textwidth]{gfx/NumerickoIntegriranje/trapz_01.pdf}
 % kate.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
 \caption{Računanje površine ispod krivulje pomoću trapeza (Izvorni kod~\ref{lst:integr_trapezna})}
 \label{fig:integr_trapezna}
\end{figure}

\section{Simpsonove formule}
\index{Simpsonove formule}
\index{Simpsonova pravila}

Simpsonova pravila (ili Simpsonove formule) koriste kvadratni ili kubni polinom za aproksimaciju funkcije prilikom numeričke integracije. Zbog nemogućnosti određivanja aproksimacijkog polinoma na jendom intervalu (dvije točke), Simpsonove formule se korsite obuhvaćajući dva podinervala (za aproksimaciju kvadratnim polinomom) ili na tri podintervala (za aproksimaciju kubnim polinomom).
\marginpar{Simposonova 1/3 formula je definirana na parovima podintervala, što uvjetuje da za njeno korištenje područje određenog integrala $[a,b]$ treba podjeliti na paran broj podintervala.}

Za aproksimaciju kvadratnim polinomom na intervalu $[x_i, x_{i+2}]$ vrijedi:
\begin{equation}
  \int_{x_i}^{x_{i+2}} f(x) dx = \frac{h}{3}(f(x_i) + 4f(x_{i+1}) + f(x_{i+2}))
  \label{eq:simpsonova_13_formula}
\end{equation}
gdje je $x$ uniformno diskretiziran ($x_{i+2} - x_{i+1} = x_{i+1} - x_i = h$).

Izraz \eqref{eq:simpsonova_13_formula} često se naziva \emph{Simpsonova 1/3 formula} a dobiven je integracijom kvadratnog polinoma koji prolazi kroz točke $(x_i, f(x_i))$, $(x_{i+1}, f(x_{i+1}))$, $(x_{i+2}, f(x_{i+2}))$.

Višestrukom primjenom na sve parove uniformno diskretiziranih podintervala na $[a,b]$ dobiva se 
\begin{equation}
 \int_a^b f(x) dx = \frac{1}{3}h \left( f(x_0) + 
 4\sum_{i=1,3}^{n-1}{f(x_i)} + 
 2\sum_{i=2,4}^{n-2}{f(x_i)} + 
 f(x_n) \right) 
 \label{eq:simpsonova_13_formula_sum}
\end{equation}
poznato kao \emph{kompozitno Simpsonovo pravilo}.

Kompozitno pravilo se može zapisati i pomoću vektorskog produkta:
\begin{equation}
 \int_a^b f(x) dx = \frac{h}{3}(\mathbf{c}^T\mathbf{f})
\end{equation}
gdje je $\mathbf{c}=[1~4~2~4~2~...~2~4~1]^T$ i $\mathbf{f}=[f_1~f_2~f_3~...~f_n]^T$.

Povećanjem stupnja aproksimacijkog polinoma potrebno je više točaka za njegovo definiranje. Za aproksimacijom polinomom 3 stupnja potrebne su četri točke tj. 3 intervala. Integriranjem polinoma dobiva se izraz za aproksimaciju određenog integrala:
\begin{equation}
  \int_{x_i}^{x_{i+3}} f(x) dx = \frac{3}{8}h(f(x_i) + 3f(x_{i+1}) + 3f(x_{i+2})+f(x_{i+3}))
  \label{eq:simpsonova_38_formula}
\end{equation}
koji se naziva \emph{Simpsonova 3/8 formula}.

\index{simps}
Scipy ima implementiranu Simpsonovu 1/3 formulu u funkciji \texttt{simps}: 
\begin{lstlisting}[language=Python, style=Syntax, keywords={simps}]
scipy.integrate.simps(y, x=None, dx=1, even='avg')
\end{lstlisting}
Funkciju \texttt{simps} se može koristit za poznate vrijednosti podintegralne funkcije \texttt{y} u točkama \texttt{x} ili za razmak \texttt{dx} uniformne diskretizacije. Argumentom \texttt{even} određuje se način na koji se računa određeni integral nad parnim brojem točaka (neparni broj podintervala):
\begin{itemize}
 \item \texttt{even = 'first'}, prvi podinterval se računa pomoću trapezne formule
 \item \texttt{even = 'last'}, zadnji podinterval se računa pomoću trapezne formule
 \item \texttt{even = 'avg'}, rezultat integracije je prosječna vrijednost integrala izračunatih sa opcijom \texttt{even = 'first'} i \texttt{even = 'last'}.
\end{itemize}

U izvornom kodu \ref{lst:integr_simpson_01} dan je primjer računanja određenog integrala pomoću Simpsonove 1/3 formule. 

\lstinputlisting[language=Python,
         style=File,
         caption={Upotreba Simpsonove 1/3 formule},
         label=lst:integr_simpson_01]
         {gfx/NumerickoIntegriranje/simpson_01.py}
      
\begin{lstlisting}[language=Terminal, style=Terminal]
351.089170175
351.089170175
\end{lstlisting}

\section{Naprednije metode}
   
Scipy nudi funkciju \texttt{quad} koja koristi napredniju adaptivnu metodu za numeričku integraciju.   
\index{quad}
\marginpar{\texttt{quad} se zansiva na QUADPACK-u - Fortran biblioteci za numeričku integraciju jednodimenzionalnih funkcija.}

Osnovna upotreba funkcije \texttt{quad} zahtjeva podintegralnu funkciju \texttt{f} te granice određenog integrala \texttt{a} i \texttt{b}.
\begin{lstlisting}[language=Python, style=Syntax, keywords={quad}]
scipy.integrate.quad(f, a, b) 
\end{lstlisting}

U izvornom kodu \ref{lst:integr_quad_01} rješen je isti određeni integral kao i u \ref{lst:integr_simpson_01}.
\lstinputlisting[language=Python,
         style=File,
         caption={Numeričko integriranje pomoću \texttt{quad} funkcije},
         label=lst:integr_quad_01]
         {gfx/NumerickoIntegriranje/quad_01.py}
      
\begin{lstlisting}[language=Terminal, style=Terminal]
351.292211631 
\end{lstlisting}

\section{Zadaci}
\begin{exercise}
Lim je izrezan u obliku štita koji je omeđen:
\begin{itemize}
 \item parabolom $y = x^2$
 \item parabolom $y = 2 - 0.1x^2$
\end{itemize}
Kolika je površina izrezanog lima?
\end{exercise}

\begin{exercise}
Oborinske vode sa krova zgrade odlaze u spremnik (cisterna). Na ulazu u spremnik postavljen je mjerač protoka.
Tijekom 15 minutnog pljuska svake minute zabilježen je protok oborinske vode.

 % @{\extracolsep{\fill}}
 ~\newline
\begin{tabular*}{0.8\textwidth}{cccc}
\hline
Vrijeme [min] & Protok [l/s] & Vrijeme [min] & Protok [l/s] \\
\hline
0  & 0.0   & 8  & 10.6 \\
1  & 0.0   & 9  & 11.2 \\
2  & 2.8   & 10 & 10.0 \\
3  & 4.2   & 11 & 9.5 \\
4  & 6.9   & 12 & 4.3 \\
5  & 10.1  & 13 & 1.8 \\
6  & 10.5  & 14 & 0.2 \\
7  & 11.0  & 15 & 0.0 \\
\hline
\end{tabular*}
~\newline

Ako vrijedi
\begin{equation*}
 V = \int_0^T{Q(t)} dt
\end{equation*}
koliki je ukupni volumen vode koja je ušla u spremnik?
\end{exercise}
