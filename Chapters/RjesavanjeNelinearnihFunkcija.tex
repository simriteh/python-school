
\chapter{Rješavanje nelinearnih jednadžbi} % Chapter title
\label{ch:rjesavanje_nelinearnih_jednadzbi} % For referencing the chapter elsewhere, use \autoref{ch:mathtest}

% http://docs.scipy.org/doc/scipy/reference/optimize.html

Cilj numeričkih metoda za rješavnje nelinearnih jednadžbi (eng. \emph{root finding}) je odrediti nultočku neprekidne realne funkcije $f:\mathbb{R}\to\mathbb{R}$ to jest riješiti jednadžbu
\begin{equation}\label{nelineq}
f(x)=0.
\end{equation}

Moguće je da funkcija $f$ ima više realnih nultočaka i da su neke višestruke. Također, funkcija $f$ možda  uopće nema realnih nultočaka.

Ako za neprekidnu funkciju vrijedi \[f(a)\,f(b)\leq 0\] tada funkcija na intervalu $[a,b]$ mijenja predznak pa postoji barem jedna nultočka na odabranom intervalu.
Ako je poznato da je funkcija i monotona na intervalu $[a,b]$ tada  je nultočka jedinstvena.


Osnovni korak numeričkih metoda je separirati jedan korijen $\xi$ jednadžbe (\ref{nelineq}).
Nakon što smo izolirali jedno rješenje, nekom odabranom numeričkom metodom formiramo niz aproksimacija $x_i\to \xi$. Numeričku metodu zaustavljamo kada je zadovoljen zadani kriterij točnosti 
\[|\xi-x_i|<\epsilon.\]

Postoji više numeričkih metoda za rješavanje nelinearnih funkcija, a nekoliko najznačajnijih će biti obrađeno u ovom poglavlju. Numeričke metode se razlikuju po stabilnosti, načinu primjene i brzini konvergencije (\emph{red konvergencije}).

\begin{definition}%\label{redkonvergencije}
Neka niz $(x_i)$, dobiven nekom iterativnom metodom, konvergira prema 
$\xi$ Ako postoje dvije pozitivne konstante $C, r$ takve da vrijedi
\[\displaystyle\lim_{i\to\infty} \dfrac{|\xi-x_{i+1}|}{|\xi-x_i|^r}\leq C\]
tada kažemo da metoda ima \textbf{red konvergencije} $r$.
\end{definition}

S obzirom na red konvergencije $r$, iterativna numerička metoda konvergira:
\begin{enumerate}
\item linearno ako je $r= 1$,
\item superlinearno ako je $1< r<2$,
\item kvadratično ako je $r=2$,
\item kubično ako je $r=2$.
\end{enumerate}

%----------------------------------------------------------------------------------------
\section{Metoda bisekcije}
\label{sec:metoda_bisekcije}
\index{bisekcija}
\index{bisect}

Metoda bisekcije namjenjena je rješavnju jednadžbe $f(x)=0$ na intervalu $[x_l,x_d]$ koristeći ograđivanje (eng. \emph{bracketing})
Da bi se metoda mogla primjeniti, funkcija $f(x)$ mora biti neprekidna te mora vrijediti $f(x_l) \cdot f(x_d) \leq 0$. Zbog navedenih uvjeta, funkcija $f(x)$ ima bar jedan korijen $x_0$ na intervalu $[x_l,x_d]$.

Biskecija je jedna od metoda ograđivanja, u kojoj se u svakom koraku računa vrijednost funkcije na polovici intervala. 

\begin{equation*}
 x_s = \frac{x_l+x_d}{2}
\end{equation*}

Na temelju vrijednosti funkcije u rubnim točkama te u centralnoj točki, određuje se novi, dvostruko uži, interval. Ako je $f(x_l)\cdot f(x_s) < 0$ tada je $x_0\in\left[x_l,x_s\right]$, a ako je $f(x_s)\cdot f(x_d) < 0$ tada je $x_0\in\left[x_s,x_d\right]$.

Prepolavljanje se iterativno ponavlja (Slika~\ref{fig:bisekcija_skica}). U slučaju da je $f(x_s)=0$, tada je pronađen traženi korijen funkcije.

\begin{figure}[h]
 \centering
 \includegraphics[width=\textwidth]{gfx/RjesavanjeNelinearnihFunkcija/bisekcija.pdf}
 % kate.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
 \caption{Skica metode biskecije}
 \label{fig:bisekcija_skica}
\end{figure}

Metoda biskecije, ako su navedeni uvjeti zadovoljeni, će sigurno konvergirati u jedan od korijena funkcije $f(x)$ na intervalu $[x_l,x_d]$.  
Apsolutna greška se prepolavlja u svakom koraku iteracije pa metoda konvegira linearno, što je, u odnosu na druge metode, relativno sporo.


Osnovna sintaksa:
\begin{lstlisting}[language=Python, style=Syntax, morekeywords={bisect}]
bisect(f, a, b)
\end{lstlisting}
gdje je \texttt{f} funkcija, a \texttt{a} i \texttt{b} su granice intervala na kojem se traži korijen funkcije.

\marginpar{\texttt{optimize.bisect} poziva \texttt{\_zeros.\_bisect}, koja je implementirana u C-u.}

Dodatni argumenti omogućuju kontrolu i pregled konvergencije:
\begin{lstlisting}[language=Python, style=Syntax, morekeywords={args, xtol, rtol, maxiter, full\_output, disp}]
args=()
xtol=1e-12
rtol=4.4408920985006262e-16
maxiter=100
full_output=False
disp=True
\end{lstlisting}
gdje je \texttt{xtol} apsolutna tolerancija, \texttt{rtol} relativna tolerancija, \texttt{maxiter} maksimalni broj iteracija, \texttt{full\_output} omogućuje dohvaćanje dodatnih informacija o konvergenciji i \texttt{disp} omogućuje podizanje RuntimeError u slučaju da metoda ne konvergira.


\lstinputlisting[language=Python, style=File, caption={Primjer pronalaženja nultočke funkcije metodom biskecije},label=lst:biskecija]{gfx/RjesavanjeNelinearnihFunkcija/bisekcija.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
6.77609231632
41
43
\end{lstlisting}

%----------------------------------------------------------------------------------------
\section{Newton-Raphsonova metoda}
\label{sec:newton-raphsonova_metoda}
\index{Newtonova metoda}
\index{Newton-Raphsonova metoda}
\index{metoda tangente}
\index{newton}

Za razliku od nelinearne funkcije, traženje nultočke linearne funkcije je bitno lakši zadatak. Osnovna ideja Newton-Raphsonove metode je lokalna aproksimacija nelinearne funkcije $f(x)$ najboljom linearnom aproksimacijom. Ako je poznata derivacija funkcije $f(x)$ tada je u blizini $x_1$ moguće funkciju $f(x)$ aproksimirati tangentom 
\begin{equation*}
f(x)\approx T(x)=f(x_1)+f'(x_1)(x-x_1).
\end{equation*}
$x_1$ je početna aproksimacija nultočke, a $x_0-f(x_1)/f'(x_1)$ (nultočka tangente $T(x)$) bi trebala biti bolja aproksimacija nultočke. Niz aproksimacija 
\begin{equation}
 x_{i+1} = x_i - \frac{f(x_i)}{f'(x_i)}
 \label{eq:tangenta}
\end{equation}
bi trebao konvergirati nultočki funkcije $f(x)$. \marginpar{Newthon-Raphsonova metoda je poznata i kao Newtonova metoda ili metoda tangente}. Za ovu iterativnu metodu potrebno je zadati dobru početnu aproksimaciju nultočke i derivaciju funkcije  (Slika~\ref{fig:newton-raphsonova_metoda}).

\begin{figure}[h]
 \centering
 \includegraphics[width=\textwidth]{gfx/RjesavanjeNelinearnihFunkcija/newton-raphsonova_metoda.pdf}
 % kate.png: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
 \caption{Newton-Raphsonova metoda}
 \label{fig:newton-raphsonova_metoda}
\end{figure}

Osnovna sintaksa:
\begin{lstlisting}[language=Python, style=Syntax, morekeywords={newton}]
newton(func, x0, fprime)
\end{lstlisting}
gdje je $func$ funkcija, $x0$ početna aproksimacija nultočke i $fprime$ prva derivacija funkcije (zadaje se kao funkcija). Dodatni argumenti omogućuju kontrolu i konvergencije:

\begin{lstlisting}[language=Python, style=Syntax, morekeywords={args, tol, maxiter, fprime2}]
args=()
tol=1e-12
maxiter=100
fprime2=None
\end{lstlisting}
gdje je \texttt{tol} apsolutna tolerancija, \texttt{maxiter} maksimalni broj iteracija i $fprime2$ je druga derivacija funkcije koja omogućuje ubrzanje konvergencije.

\lstinputlisting[language=Python, style=File, caption={Primjer pronalaženja nultočke funkcije metodom tangente},label=lst:tangenta]{gfx/RjesavanjeNelinearnihFunkcija/tangenta.py}
\begin{lstlisting}[language=Terminal, style=Terminal]
1.41421356237
\end{lstlisting}

Newton-Raphsonova metoda konvergira kvadratično pa je znatno je brža od metode bisekcije, ali je bisekcija stabilnija i robusnija. Iz jednadžbe \eqref{eq:tangenta} može se zaključiti da će Newton-Raphsonova metoda  biti nestabilna kada je $f'(x)\approx 0$ odnosno kada je tangenta skoro horizontalna. Također, $f'(x)$ u mnogim slučajevima nije poznata ili je vrlo zahtjevna za računanje. 




\section{Metoda sekante}
\label{sec:metoda_sekante}
\index{metoda sekante}
\index{secant}

Kada derivacija funkcije nije poznata tada je moguće modificirati metodu tangente na način da se funkcija $f(x)$ aproksimira nekom sekantom koja dobro aproksimira tangentu. U tom slučaju moraju se zadati dvije početne aproksimacije nultočke funkcije, ali derivacija funkcije nije potrebna jer je moguće aproksimirati derivaciju:
\begin{equation*}
f(x)\approx S(x)=f(x_2)+\frac{f(x_2)-f(x_1)}{x_2-x_1}(x-x_2).
\end{equation*}
Odnosno moguće je formirati niz aproksimacija 
\begin{equation}\label{tangenta}
 x_{i+1} = x_i - \frac{f(x_i)(x_i-x_{i-1})}{f(x_i)-f(x_{i-1})}.
\end{equation}

Osnovna sintaksa:
\begin{lstlisting}[language=Python, style=Syntax, morekeywords={newton}]
newton(func, x0)
\end{lstlisting}
gdje je $func$ funkcija, a $x0$ početna aproksimacija. 

Metoda sekante je nešto sporija od metode tangenete, ali je ipak znatno brža od bisekcije. Kažemo da sekanta konvergira superlinearno. Također, metoda sekante postaje nestabilna u slučaju kada je sekanta skoro horizontalna.

%----------------------------------------------------------------------------------------
\section{Brentova metoda}
\label{sec:brentova_metoda}
\index{Brentova metoda}
\index{brent}

Osnovna sintaksa:
\begin{lstlisting}[language=Python, style=Syntax, morekeywords={brent}]
brent(func, a, b)
\end{lstlisting}


\clearpage
\section{Zadaci}



\begin{exercise}
Lopta (sfera) je uronjena u vodu i postiže stanje ravnoteže.
\vspace*{1em}
\begin{center}
\includegraphics[width=0.5\textwidth]{gfx/RjesavanjeNelinearnihFunkcija/example_01.pdf}
\end{center}
Volumen uronjenog dijela lopte iznosi
\begin{equation*}
V(H) = \int_0^H \pi r(h)^2 dh
\end{equation*}
\begin{equation*}
V(H) = \int_0^H R^2 - (h-R)^2 dh = \int_0^H 2Rh - h^2 dh
\end{equation*}
\begin{equation*}
V(H) = RH^2 - \frac{H^3}{3}
\end{equation*}

Uzgon koji daje uronjeni dio lopte iznosi
\begin{equation*}
U(H) = V(H) \cdot \rho \cdot g
\end{equation*}

U stanju ravnoteže, uzgon je jednak težini lopte.
\begin{equation*}
U(H) = m \cdot g
\end{equation*}
\begin{equation*}
RH^2 - \frac{H^3}{3} = m \cdot g
\end{equation*}

Lopta mase $m=0.4 kg$ i promjera $R=0.22m$ stavljena je u vodu gustoće $\rho=1024kg/m^3$. Koliko će lopta biti uronjena (u stanju ravnoteže)?
\end{exercise}