
\chapter{Rad sa datotekama}
\label{ch:rad_sa_datotekama}
\index{datoteke}

\section{Otvaranje i zatvaranje datoteka}

Osnovna manipulacija sa sadržajem datoteka u Pythonu je omogućena korištenjem \texttt{file}\index{file} objekta.

Python ima ugrađenu funkciju \texttt{open}\index{open} za otvaranje postojećih ili stvaranje novih datoteka na disku. Funkcija \texttt{open} vraća \texttt{file} objekt, koji sadrži metode i atribute za pristup i manipulaciju informacija i sadržaja otvorene datoteke.

Sintaksa otvaranja datoteka je:
\begin{lstlisting}[language=Python, style=Syntax]
f = open(ime\_datoteke, vrsta\_pristupa, buffering)
\end{lstlisting}
gdje je \texttt{ime\_datoteke} sting koji sadrži relativnu ili apsolutnu putanju datoteke, \texttt{vrsta\_pristupa} je string koji sadrži mod pristupa, \texttt{buffering} je integer koji označava veličinu međupohrane. Argumenti \texttt{vrsta\_pristupa} i \texttt{buffering} nisu obavezni. \texttt{f} je novonastali \texttt{file} objekt kreiran pozivom funkcije \texttt{open}.

\marginpar{Početna pozicija unutar datoteke razlikuje se ovisno o opciji \texttt{'r'}, \texttt{'w'} (početak datoteke) ili \texttt{'a'} (kraj datoteke).}

Mod pristupa se definira kao string koji sadrži znakove:
\begin{itemize}
 \item \texttt{'r'}, \texttt{'w'} ili \texttt{'a'} kojima se definira da li će se vršiti čitanje (\emph{read}), pisanje (\emph{write}) ili dodavanje (\emph{append})
 \item \texttt{'b'} označava binarni oblik datoteke. Ako nije specificiran onda je oblik datoteke takstualni (\texttt{'t'}).
 \item \texttt{'+'} omogućuje istodobno čitanje i pisanje (ili dodavanje) datoteke. 
\end{itemize}


Nakon obavljanja željenih manipulacija, koje su objašnjene u narednim poglavljima, datoteku je potrebno zatvoriti. Za zatvaranje datoteke koristi se \texttt{close}\index{close} metoda koju ima svaki \texttt{file} objekt:
\begin{lstlisting}[language=Python, style=Syntax]
f.close()
\end{lstlisting}

Nakon zatvaranja nisu moguće nikakve operacije na datoteci.

\section{Pisanje u datoteke}

Za zapisivanje informacija u datoteku koriste se metode \texttt{write}\index{write} i \texttt{writelines}\index{writelines} klase \texttt{file}.

\texttt{write} metoda prima string varijablu koju zapisuje u prethodno otvorenu datoteku u modu za pisanje (\texttt{'w'}) ili dodavanje (\texttt{'a'}). Ovu naredbu može se ponavljati kako bi se dodalo još teksta u datoteku.
\begin{lstlisting}[language=Python, style=Syntax, keywords={write}]
f.write(tekst)
\end{lstlisting}

\lstinputlisting[language=Python,
                 style=File,
                 caption={Primjer kreiranja tekstualne datoteke},
                 label=lst:rad_sa_datotekama_pisanje]
                 {gfx/RadSaDatotekama/pisanje.py}
                 

\lstinputlisting[language=Plain,
                 style=PlainFile]
                 {gfx/RadSaDatotekama/info.txt}
                 
Funkcija \texttt{writelines} zapisuje listu stringova u datoteku. Postignuti efekt je isti kao da se pozove \texttt{write} za svaki element liste. \marginpar{Iako njeno ime na to upućuje, \texttt{writelines} ne zapisuje svaki element liste u novi redak!}

\begin{lstlisting}[language=Python, style=Syntax, keywords={writelines}]
f.writelines(lista_stringova)
\end{lstlisting}
\begin{lstlisting}[language=Python, style=Terminal]
l = ['malo', ' ', 'teksta']

f.writelines(l)

# isto što i
for s in l:
    f.write(s)
\end{lstlisting}
               
\section{Čitanje iz datoteka}

Za čitanje datoteke potrebno je željenu datoteku otvoriti u modu za čitanje (\texttt{'r'}).
Osnovni način za pročitati sadržaj određene datoteke je preko funkcija:
\begin{itemize}
 \item \texttt{read}\index{read} koja vraća sadržaj datoteke (od trenutne pozicije) željene veličine u obliku stringa,
 \item \texttt{readline}\index{readline} koja vraća sadržaj trenutne linije u obliku stringa,
 \item \texttt{readlines}\index{readlines} koja vraća listu stringova koji predstavljaju pojedine linije datoteke
\end{itemize}

Sintaks metode \texttt{read} je:
\begin{lstlisting}[language=Python, style=Syntax, keywords={read}]
f.read(size)
\end{lstlisting}
gdje je \texttt{size} veličina sadržaja koji treba pročitati. Za binarnu datoteku \texttt{size} označava broj bitova a za tekstualnu broj znakova. Ukoliko je size negativan ili nije zadan, čita se cijeli sadržaj datoteke.
\marginpar{Kod čitanja datoteka vrlo često je bitna trenutna pozicija u samoj datoteci što je objašnjeno u poglavlju \ref{sec:pozicioniranje_u_datoteci}}
\begin{lstlisting}[language=Python, style=Syntax, keywords={readline}]
f.readline(size)
\end{lstlisting}
Metoda \texttt{readline} čita jednu liniju u datoteci i to od trenutne pozicije do kraja lnije (do znaka \texttt{'\\n'}). Argument \texttt{size} definira maksimalnu veličinu pročitanih podataka. Za prazne lnije metoda vraća string koji sadrži znak za novu liniju \texttt{'\\n'}. Kad je trenutna pozicija na kraju datoteke, metoda vraća prazan string.

\begin{lstlisting}[language=Python, style=Syntax, keywords={readlines}]
f.readlines(size)
\end{lstlisting}
\texttt{readlines} vraća sadržaj datoteke kao listu linija. \texttt{size} ima jednak učinak kao i na dvije prethodno objašnjene metode.

Za potrebe primjera, zadana je datoteka \emph{test.txt} sljedećeg sadržaja:
\lstinputlisting[language=Plain,
                 style=PlainFile]
                 {gfx/RadSaDatotekama/test.txt} 

U izvornom kodu \ref{lst:rad_sa_datotekama_citanje_01} dani su primjeri upotrebe navedenih funkcija za čitanje sadržaja datoteke.
\lstinputlisting[language=Python,
                 style=File,
                 caption={Čitanje sadržaja tekstualne datoteke},
                 label=lst:rad_sa_datotekama_citanje_01]
                 {gfx/RadSaDatotekama/citanje_01.py} 
\begin{lstlisting}[language=Terminal, style=Terminal]
12345
6789

ABCDEF
['GHIJKLMNOPQRSTUVWXYZ\n', '3. linija\n', '4. linija\n', 'zadnja!']
\end{lstlisting}

Osim pomoću navedenih funkcija, postoji još jedan način pristupanju linijama neke tekstualne datotetke, a to je iterirajući kroz file objekt kao što je prikazano u izvornom kodu \ref{lst:rad_sa_datotekama_citanje_03}.

\lstinputlisting[language=Python,
                 style=File,
                 caption={Pristupanje sadržaju datoteke preko iteriranja po \texttt{file} objektu},
                 label=lst:rad_sa_datotekama_citanje_03]
                 {gfx/RadSaDatotekama/citanje_03.py} 
\lstinputlisting[language=terminal,
                 style=terminal]
                 {gfx/RadSaDatotekama/test.txt} 


\section{Pozicioniranje u datoteci}
\label{sec:pozicioniranje_u_datoteci}

Trenutnu poziciju u datoteci može se saznati preko metode \texttt{tell}\index{tell}:
\begin{lstlisting}[language=Python, style=Syntax, keywords={tell}]
f.tell()
\end{lstlisting}

Metoda \texttt{tell} daje broj bitova (znakova) trenutne pozicije od početka datoteke (Izvorni kod \ref{lst:rad_sa_datotekama_citanje_02}). 

\texttt{seek}\index{seek} momogućuje micanje trenutne pozicije u datoteci. Sintaksa metode je:
\begin{lstlisting}[language=Python, style=Syntax, keywords={seek}]
f.seek(offset, from)
\end{lstlisting}
\texttt{offset} je broj koim definiramo pomak pozicije. Pozitivan je u naprijed a negativan u nazad. Argumentom \texttt{from} se definira od kuda će se pomak računati (0 za početak datoteke, 1 za trenutni položaj i 2 za kraj datoteke). 

\lstinputlisting[language=Python,
                 style=File,
                 caption={Dohvaćanje trenutne pozicije u datoteci},
                 label=lst:rad_sa_datotekama_citanje_02]
                 {gfx/RadSaDatotekama/citanje_02.py} 
\begin{lstlisting}[language=Terminal, style=Terminal]
123456789

pozicija: 10
ABC
pozicija: 13
JKL
JKLMN
12345
dnja!
\end{lstlisting}

\section{Preimenovanje, kopiranje i brisanje datoteka}

\index{rename}
\index{copy}
\index{move}
\index{shutil}
\index{os}

\url{http://docs.python.org/2/library/shutil.html}

\lstinputlisting[language=Python,
                 style=File,
                 caption={Kopiranje, micanje i preimenovanje datoteka},
                 label=lst:copy_rename_move]
                 {gfx/RadSaDatotekama/copy_rename_move.py} 
                 
Paket \texttt{os} omogućuje manipulaciju datotekama na disku. Neke od korisnih naredbi za stvaranje direktorija i brisajne su:
\begin{itemize}
\item \texttt{makedirs()} omougućuje stvaranje direktorija
\item \texttt{getcwd()} vraća tekući direktorij u obliku stringa
\item \texttt{chdir()} mijenja tekući direktorij
\item \texttt{rename()} omogućuje preimenovanje datoteke
\item \texttt{removedirs()} briše prazne direktorije
\item \texttt{remove()} briše datoteku
\end{itemize}   
          
\lstinputlisting[language=Python,
                 style=File,
                 caption={Stvaranje datoteka i direktorija},
                 label=lst:stvaranje]
                 {gfx/RadSaDatotekama/stvaranje_datoteka_direktorija.py} 

Kod korištenja naredbi za manipulaciju datotekama, moramo biti svjesni operativnog sustava na kojem radimo. Primjer \ref{lst:stvaranje} napisan je za linux OS pa bi trebalo obrnuti kosu crtu kod popisa direktrija kako bi se prilagodilo Windows OS-u.

S brisanjem direktorija i datoteka moramo biti malo više oprezni pa \texttt{removedirs()} naredba vraća pogrešku ako pokušamo obrisati direktorij koji nije prazan.

                
\lstinputlisting[language=Python,
                 style=File,
                 caption={Preimenovanje i brisanje datoteka i direktorija},
                 label=lst:brisanje]
                 {gfx/RadSaDatotekama/brisanje_datoteka.py}                  
\section{Arhiviranje}

\begin{lstlisting}[language=Python, style=File, keywords={tell}]
from shutil import make_archive
import os
archive_name = os.path.expanduser(os.path.join('~', 'myarchive'))
root_dir = os.path.expanduser(os.path.join('~', '.ssh'))
make_archive(archive_name, 'gztar', root_dir)
'/Users/tarek/myarchive.tar.gz'
\end{lstlisting}
