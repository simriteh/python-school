
\chapter{Početni problemi} % Chapter title
\label{ch:pocetni_problemi} % For referencing the chapter elsewhere, use \autoref{ch:mathtest}

\section{Modeliranje otvorenog vodotoka}
%\label{ch:tufte-design}


Promatrajmo strujanje fluida u otvorenom vodotoku\index{otvoreni vodotok}\cite{chaudry2008openchannelflow,szymkiewicz2010numerical} (eng. \emph{open channel flow}\index{open channel flow}). Poprečni presjeci kanala mogu biti različiti npr. pravokutni, trapezni i sl., dok se kod prirodnih vodotoka pojavljuju poprečni presjeci nepravilnih  oblika. Ukoliko je poprečni presjek kanala jednak na proizvoljnom presjeku, te je nagib dna jednak u bilo kojoj točki, 
govorimo o prizmatičnom kanalu. 

\begin{figure}[H]
  \centering
  \includegraphics[keepaspectratio=true, width=0.5\textwidth]{gfx/PocetniProblemi/open_channel-01.pdf}
%  \checkparity This is an \pageparity\ page.%
  \caption{Skica poprečnog presjeka otvorenog vodotoka}
  \label{fig:open_channel-cross_section}
  %\zsavepos{pos:textfig}
  %\setfloatalignment{b}
\end{figure}

Na poprečnom presjeku kanala definiramo sljedeće veličine:
\begin{itemize}
  \item $A$ - površina omočenog presjeka kanala ($A=A(x,y)$
  \item $P$ - perimetar odnosno opseg omočenog presjeka ($P=P(x,y)$) \index{perimetar}
  \item $R$ - hidraulički radijus ($R=\dfrac{A}{P}$)
\end{itemize}

Klasifikacija strujanja se može napraviti prema različitim kriterijima. Ako se brzina strujanja ne  mijenja u ovisnosti o vremenu, onda je strujanje  stacionarno\index{stacionarno strujanje}, u suprotnom se radi o nestacionarnom  strujanju\index{nestacionarno strujanje}. Nadalje, ako se brzina strujanja ne mijenja s obzirom na udaljenost u kanalu (od neke referentne točke) onda je strujanje uniformno\index{uniformno strujanje}, a inače se radi o neuniformnom strujanju\index{neuniformno strujanje}.  


\begin{figure}[H]
  \centering
  \includegraphics[keepaspectratio=true, width=0.5\textwidth]{gfx/PocetniProblemi/open_channel-02.pdf}
%  \checkparity This is an \pageparity\ page.%
  \caption{Energetske linije u strujanju fluida otvorenim vodotokom}
  \label{fig:open_channel-energy_lines}
  %\zsavepos{pos:textfig}
  %\setfloatalignment{b}
\end{figure}

Pretpostavke:
\begin{itemize}
\item kut $\Phi$ je mali
\item strujanje je izrazito jednodimenzionalno
\item tlak je isključivo hidrostatski
\end{itemize}

Općenito se strujanje u kanalu opisuje sustavom parcijalnih diferencijalnih jednadžbi, koje proizlaze iz zakona očuvanja mase i zakona očuvanja količine gibanja. Ako pretpostavimo da se radi o prizmatičnom kanalu, možemo ih zapisati u obliku: 

\begin{equation}
  \dfrac{\partial A}{\partial t} + \dfrac{\partial Q}{\partial x} = 0
  \label{eq:ocuvanjemase}
\end{equation}
\begin{equation}
  \dfrac{\partial v}{\partial t} + g\dfrac{\partial}{\partial x}\left(y+\dfrac{v^2}{2g}\right) = g(S_0-S_f)
\end{equation}

$y$ je dubina vode, $v$ brzina, a $Q$ protok, pri čemu vrijedi relacija $v = \dfrac{Q}{A}$. $S_f$ je pad energetske linije $\dfrac{dH}{dx}$, definiran Manningovom formulom \index{Manningova formula} (koja je prikazana u nastavku). Uvedimo oznaku

\begin{equation}
  \dfrac{dH}{dx} = -S_f
\end{equation}

S $S_0$ smo označili nagib dna kanala, tj. $S_0=-\dfrac{dz}{dx}=-tan(\Phi)$.

\subsection{Stacionarno strujanje}
\index{stacionarno strujanje}
Nadalje, ukoliko promatramo stacionarno strujanje fluida, očito je da zbog neovisnosti varijabli o vremenu tj.  0 $\dfrac{\partial A}{\partial t}=0$ i $\dfrac{\partial v}{\partial t}=0$, iz prve jednadžbe sustava \eqref{eq:ocuvanjemase} slijedi: 
\begin{equation}
 Q = konst.
\end{equation} 
odnosno iz druge jednanžbe
\begin{equation}
 \dfrac{dz}{dx}+\dfrac{dy}{dx}+\dfrac{d}{dx}\left(\dfrac{v^2}{2g}\right)=S_f
\end{equation} 

Zapravo se ova jednadžbe može dobiti i ako krenemo od poznate nam Bernoullijeve jednažbe
\begin{equation}
 z + \dfrac{p}{\rho g} + \dfrac{v^2}{2g} = H
\end{equation}
prema kojoj je ukupna energija  konstantna duž strujnice. Vrijednost energetske linije označimo s  H.
Uvažavanjem jednakosti  $p = \rho g y$ , slijedi $z+y+\dfrac{v^2}{2g}=H$, odnosno deriviranjem po $x$ dobijemo diferencijalnu jednadžbu za dubinu vode $y = y(x)$:
\begin{equation}
 \dfrac{dz}{dx}+\dfrac{dy}{dx}+\dfrac{d}{dx}\left(\dfrac{Q^2}{2gA^2}\right)=\dfrac{dH}{dx}
\end{equation} 

Nadalje, kod stacionarnog toka ($Q = konst.$) u prizmatičnom kanalu vrijedi
\begin{equation}
\begin{aligned}
  \dfrac{d}{dx}\left(\dfrac{Q^2}{2gA^2}\right)&=\dfrac{Q^2}{2g}\dfrac{d}{dx}\left(\dfrac{1}{A^2}\right)\\
  &=\dfrac{Q^2}{2g}\dfrac{d}{dy}\left(\dfrac{1}{A^2}\right)\dfrac{dy}{dx}\\
  &=-\dfrac{Q^2}{gA^3}\dfrac{dA}{dy}\dfrac{dy}{dx}\\
  &=-\dfrac{Q^2B}{gA^3}\dfrac{dy}{dx}
\end{aligned}
\end{equation} 

Kod druge smo jednakosti uvažili činjenicu da je kanal prizmatičan, što znači da se poprečni profili ne mijenjaju s obzirom na udaljenost  x u kanalu, dok zadnja jednakost slijedi iz $\dfrac{dA}{dy}=B$. Pad energetske linije $\dfrac{dH}{dx}=-S_f$ definiran je Manningovom formulom, pomoću koje se modeliraju gubici nastali zbog trenja

\subsection{Maningova formula}
\index{Manningova formula}
Manningova formula, još poznata kao Gauckler–Manningova formula ili Gauckler–Manning–Stricklerova formula, je empirijska jednadžba za strujanje u otovrenim kanalima ili strujanja sa slobodnom površinom pod utjecajem gravitacije. Definirana je kao:
\begin{equation}
 S_f=\dfrac{Q^2n^2}{R^{4/3)A^2}}
 \label{eq:manningova_formula}
\end{equation} 

gdje je  $n$  Manningov koeficijent trenja\index{Manningov koeficijent trenja}, $R=\dfrac{A}{P}$ je  hidraulički radijus, a $P$ perimetar.

\begin{table}
  \centering
  \fontfamily{ppl}\selectfont
  \begin{tabular}{ll}
    \toprule
    Vrsta kanala & Manningov koeficijent\\
    \midrule
    Betonski kanal & 0.015\\
    Prirodni kanali (glina) & 0.030\\
    Riječni tokovi & 0.030 - 0.070\\
    \bottomrule
  \end{tabular}
  \caption{Vrijednosti Manningovog koeficijenta za različite izvedbe kanala.}
  \label{tab:normaltab}
  %\zsavepos{pos:normaltab}
\end{table}

\begin{equation}
 \label{eq:stacionarna}
 \dfrac{dy}{dx}=\dfrac{S_0-S_f}{1-\dfrac{BQ^2}{gA^3}}
\end{equation} 

\marginpar{\textbf{Zadatak}: Za početni uvjet $y(x_0)=y_0$ odrediti dubinu vode $y(x)$ iz diferencijalne jednadžbe  \eqref{eq:stacionarna}.}

Opazimo da jednadžba \eqref{eq:stacionarna} vrijedi za $\dfrac{BQ^2}{gA^3} \neq 1$

\eqref{eq:stacionarna} je diferencijalna jednadžba prvog reda oblika
\begin{equation*}
 \dfrac{dy}{dx} = f(x,y(x))
\end{equation*}
za koju analitičko rješenje znamo odrediti samo u nekim posebnim slučajevima, za razliku od numeričkog rješenja koje znamo odrediti u području u kojem je diferencijalna jednadžba dobro definirana. Da bi problem bio u potpunosti modeliran potrebno je zadati i početne uvjete.

\subsection{Uniformno strujanje i normalna dubina}
\index{uniformno strujanje}
\index{normalna dubina}
Kao što smo već spomenuli, kod uniformnog strujanja su brzina i dubina vode konstantne veličine pa iz jednadžbe \eqref{eq:stacionarna}, zbog $\dfrac{dy}{dx}=0$, slijedi:
\begin{equation}
 S_0=S_f
\end{equation} 
Iz Manningove formule \eqref{eq:manningova_formula}  potom proizlazi da kod uniformnog toka vrijedi:
\begin{equation}
 \label{eq:protok}
 Q=\dfrac{1}{n}AR^{2/3}S_0^{1/2}
\end{equation} 
Na osnovu te jednadžbe može se odrediti dubina vode kod uniformnog toka, tzv. normalna dubina koju ćemo označiti $y_n$. 

\subsubsection{Prizmatični kanal s pravokutnim poprečnim presjekom}
$B$ - širina kanala

\begin{equation}
 A(y)=B\cdot y \Rightarrow \dfrac{dA}{dy}=B
\end{equation} 
\begin{equation}
 P(y)=B+2y
\end{equation} 

\begin{figure}[H]
  \centering
  \includegraphics[keepaspectratio=true, width=0.5\textwidth]{gfx/PocetniProblemi/open_channel-05.pdf}
  \caption{Pravokutni poprečni presjek kanala}
\end{figure}

Kod zadavanja podataka, obično su nam poznate veličine: $Q$, $n$, $S_0$, $B$. Normalnu dubinu potom možemo odrediti iz sljedećeg izraza:
\begin{equation}
\begin{aligned}
 Q=&\dfrac{1}{n}AR^{2/3}S_0^{1/2}\\
  =&\dfrac{1}{n}By_n\left(\dfrac{By_n}{B+2y_n}\right)^{2/3}S_0^{1/2}
\end{aligned}
\end{equation} 
koristeći neku od numeričkih metoda

\subsubsection{Trapezni kanal}


$B_0$ - širina kanala
$Z$ - nagib bočnih stranica kanala (tangens kuta nagiba bočnih stranica kanala)
\begin{equation}
 A(y)=(B_0+yZ)\cdot y \Rightarrow \dfrac{dA}{dy}=B_0+2yZ
\end{equation}
\begin{equation}
 P(y)=B_0+2y\sqrt{1+Z^2}
\end{equation}

\begin{figure}[H]
  \centering
  \includegraphics[keepaspectratio=true, width=0.5\textwidth]{gfx/PocetniProblemi/open_channel-04.pdf}
%  \checkparity This is an \pageparity\ page.%
  \caption{Trapezni poprečni presjek kanala}
  \label{fig:open_channel-trapezni_presjek}
  %\zsavepos{pos:textfig}
  %\setfloatalignment{b}
\end{figure}

\begin{exercise}
Odrediti normalnu dubinu vode $y_n$ u trapeznom kanalu, s bočnim nagibom $Z=2$, te širinom dna $B_0=10m$. Nagib dna kanala je konstantan i jednak $S_0=-\dfrac{dz}{dx}=0.001$ (1m po km duljine) a Manningov koeficijent  $n=0.013$.
\end{exercise}

\begin{solution}
Dubinu vode označimo s y.
Iz sličnosti trokuta slijedi $1:z=y:x$, pa je $x=zy$.
Površina  omočenog presjeka trapeza je $A=(B_0+zy)y$.
Opseg omočenog presjeka trapeza je $P=B_0+2y\sqrt{1+z^2}$.
Hidraulički radijus je 
\begin{equation*}
R=\dfrac{A}{P}=\dfrac{(B_0+zy)y}{B_0+2y\sqrt{1+z^2}}.
\end{equation*}

Normalnu dubinu vode potom određujemo iz jednadžbe \eqref{eq:protok}, koja nakon uvrštavanja dobivenih izraza i i sređivanja poprimi oblik
\begin{equation*}
 \dfrac{nQ}{S_0}-\dfrac{\left[\left(B_0+zy_n\right)y_n\right]^{5/3}}{\left[B_0+2y_n\sqrt{1+z^2}\right]^{2/3}}=0
\end{equation*} 
Nakon uvrštavanja poznatih vrijednosti dobijemo
\begin{equation*}
 F(y_n)=12.33(10+4.47y_n)^{2/3}-\left((10+2y_n)y_n\right)^{5/3}=0
\end{equation*} 
Nul točka funkcije $F(y)$ predstavlja rješenje gornje jednadžbe. Može se odrediti pomoću neke od numeričkih metoda npr. bisekcijom ili Newtonovom metodom. Rješenje je  
\begin{equation*}
 y_n=1.09m
\end{equation*}
\end{solution}




\subsection{Kritična dubina}
Nadalje, definirajmo kritičnu dubinu $y_c$. To je dubina kod koje je Froudov broj jednak 1, tj.
\begin{equation}
 Fr=\dfrac{v}{\sqrt{gy_c}}=1
 \label{eq:froudov_broj}
\end{equation}
Opazimo da je u slučaju kritične dubine nazivnik u diferencijalnoj jednadžbi \eqref{eq:stacionarna} jednak 0 te da diferencijalna jednadžba u toj točki ne vrijedi. Kritičnu dubinu možemo izraziti  iz \eqref{eq:froudov_broj} te dobijemo
\begin{equation}
 y_c=\sqrt[3]{\dfrac{Q^2}{gA^2}}=\sqrt[3]{\dfrac{q^2}{g}}
\end{equation}
gdje je $q=\dfrac{Q}{A}$. Karakteristika kritične dubine je da je to dubina kod koje je specifična energija 

fluida minimalna. 

Vrijednost  $c=\sqrt{gy}$ nazivamo brzina širenja poremećaja. To je zapravo relativna brzina širenja vala u odnosu na medij unutar kojeg taj val putuje. Apsolutne brzine širenja vala su zapravo jednake $v-c$ i $v+c$.  Ako je  $v<c$ onda govorimo o podkritičnom toku, za $v=c$ je tok kritičan, dok je za $v>c$ tok nadkritičan. Uočimo da kod podkritičnog toka vrijedi  $v-c<0$ , a  $v+c>0$ što znači da se jedan kraj vala putuje uzvodno, a drugi nizvodno. Kod kritičnog je toka   $v-c=0$ , pa je jedna strana vala stacionarna, dok druga putuje nizvodno brzinom  $v+c$. Za razliku od toga, kod nadkritičnog toka se obje strane vala gibaju nizvodno, jer je  $v-c>0$ i  $v+c>0$. Dakle, kod nadkritičnog toka poremećaj putuje isključivo nizvodno, što znači da tok uzvodno od promatrane lokacije uopće 'ne zna' što se događa nizvodno.

\subsection{Hidraulički skok}
\index{hidraulički skok}
Hidraulički skok (jump) se pojavljuje kod prijelaza strujanja iz nadkritičnog u pdkritično. U točki u kojoj je Froudov broj jednak 1 dogodi se hidraulički skok. Kod hidrauličkog skoka dolazi do gubitka energije (zbog turbulencije) i do naglog skoka u vodnom licu. Kako gubitak energije na skoku nije unaprijed poznat, jednadžbu očuvanja energije ne možemo primijeniti direktno. Uočimo također da u točki skoka diferencijalna jednadžba \eqref{eq:stacionarna} ne vrijedi jer je nazivnik desne strane jednak nula.

U nastavku ćemo promatrati rješenja jednadžbe \eqref{eq:stacionarna} uz postavljeni početni uvjet $y(x_0)=y_0$, tj. poznatu dubinu vode u nekoj točki. Kako analitička rješenja jednadžbe nisu poznata, osim u nekim posebnim slučajevima, za rješavanje ćemo koristiti numeričke metode. Prisjetimo se, da su standardne numeričke metode za rješavanje diferencijalne jednadžbe prvog reda: Eulerova metoda, poboljšana Eulerova metoda i različite Runge-Kutta metode. Koju ćemo od spomenutih metoda odabrati ovisi o točnosti koju želimo postići. Pritom treba svakako voditi računa i o stabilnosti pojedinih metoda.

Rješenja postavljenog početnog problema mogu biti različitog oblika, u zavisnosti o početnom uvjetu, nagibu kanala i trenju u kanalu. Promatrati ćemo različite slučajeve u tzv. kanalima s blagim nagibom (mild slope). Takav je nagib okarakteriziran uvjetom
\begin{equation}
 y_c<y_n
\end{equation}
U tom se slučaju linija kritične dubine nalazi ispod linije normalne dubine. Tim je dvjema linijama područje strujanja podijeljeno u tri zone (vidi sliku 3.). Oblik rješenja, dakle vodno lice, ovisi o početnom uvjetu. Tražena se rješenja određuju  iz diferencijalne jednadžbe \eqref{eq:stacionarna}, koja  se za promatrani pravokutni prizmatični kanal može zapisati u obliku: 
\begin{equation}
 \dfrac{dy}{dx}=S_0\dfrac{1+\left(\dfrac{y_n}{y}\right)^{10/3}}{1-\left(\dfrac{y_c}{y}\right)^3}
\end{equation}
U ovisnosti o zoni u kojoj se nalazi  početni uvjet, dakle početna dubina ovisi sami oblik rješenja. Značajno je naglasiti da dubina vode, odnosno rješenje ne može prijeći diferencijalne jednadžbe iz jedne u drugu zonu – dakle, ako je početni uvjet u zoni 1, vodno lice ostati će u zoni 1. Pripadajuća se krivulja naziva M1 krivulja.

Razlikujemo M1, M2 i M3  krivulje\index{M krivulje} (Slika \ref{fig:open_channel-M_curves}).
Vrijedi:
\begin{itemize}
 \item za $y_c<y_n<y_0$ rješenje je M1-krivulja
 \item za $y_c<y_0<y_n$ rješenje je M2-krivulja
 \item za $y_0<y_c<y_n$ rješenje je M3-krivulja
\end{itemize}


U točkama u kojima se dogodi prijelaz iz jedne u drugu zonu, rješenje zapravo ne zadovoljava diferencijalnu jednadžbu (to su slučajevi u kojima je nazivnik  u diferencijalnoj jednadžbi jednak 0, dakle točke u kojima se dostigne kritičan tok).

Primjeri u kojima se pojavljuju promatrane krivulje:
\begin{itemize}
 \item M1-krivulja – podkritično strujanje (Fr<1)  - utok u jezero;
 \item M2-krivulja – prijelaz iz podkritičnog u nadkritično strujanje kod kanala čiji se nagib poveća; strujanje prije prijelaza u nadkritično ima oblik M2-krivulje;
 \item M3-krivulja – prijelaz iz nadkritičnog u podkritično strujanje npr. strujanje ispod zapornice; dio krivulje prije prijelaza u podkritični tok ima oblik M3-krivulje.
\end{itemize}
Na sličan se način rješenja mogu dobiti i za veće nagibe kanala (steep slope) – kada je $y_c>y_n$. U tom slučaju rješenja imaju oblik tzv. S-krivulja \index{S krivulje}(Slika \ref{fig:open_channel-S_curves}).

\begin{figure}[ht]
\begin{minipage}[b]{0.45\linewidth}
  \centering
  \includegraphics[width=\linewidth]{gfx/PocetniProblemi/open_channel-03.pdf}
  \caption{M-krivulje vodnog lica}
  \label{fig:open_channel-M_curves}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.45\linewidth}
  \centering
  \includegraphics[width=\linewidth]{gfx/PocetniProblemi/open_channel-06.pdf}
  \caption{S-krivulje vodnog lica}
  \label{fig:open_channel-S_curves}
\end{minipage}
\end{figure}

\subsection{Numeričko rješavanje}

\lstinputlisting[language=Python, style=File, caption={Strujanje u pravokutnom kanalu},label=lst:open_channel]{gfx/PocetniProblemi/pravokutni_kanal.py}

\begin{figure}[h]
  \includegraphics[keepaspectratio=true, width=\textwidth]{gfx/PocetniProblemi/open_channel-07.pdf}
%  \checkparity This is an \pageparity\ page.%
  \caption{Numeričko rješenje strujanja u otvorenom kanalu pravokutnog presjeka}
  \label{fig:open_channel-solution}
  %\zsavepos{pos:textfig}
  %\setfloatalignment{b}
\end{figure}