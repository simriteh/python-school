% Poglavlje 13

%----------------------------------------------------------------------------------------

\chapter{Greške u Python kodu} % Chapter title
\label{ch:greske} % For referencing the chapter elsewhere, use \autoref{ch:examples} 

\section{Tipovi grešaka} % Chapter title
\label{sec:tipovi_gresaka} 
\index{pdb}
\index{debug}

Većina programskih jezika posjeduje određene usluge prema korisniku koje omogućuju identificiranje i manipulaciju grešaka u pisanom izvornom kodu te davaju korisna objašnjenja vezana uz izvor greške ili način kako ih ukloniti. Python slijedi takvu logiku i omogućuje prijavljivanje dva tipa grešaka:
\begin{itemize}
 \item Sintaksne greške \textit{Syntax Error} i
 \item Iznimke (eng. \textit{Exceptions})
\end{itemize}

Obje greške utvrđuju se isključivo tijekom izvođenja tj. interpretiranja koda: sintaksne greške na početku izvođenja koda, a iznimke za svaki izvršeni red koda.
Nakon što Python identificira prvu grešku na koju naiđe program stane sa izvršavanjem i objavi poruku slijedećeg izgleda
\begin{lstlisting}[language=Python, style=Terminal]
>>> print c
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'c' is not defined
\end{lstlisting}
\textit{Traceback} obavještava korisnika o tome gdje je greška pronađena i daje opis greške pomoću sintaksne greške ili iznimke (eng. \textit{exception}) koja objašnjava razlog prekida izvršavanja programa. U navedenom primjeru \textbf{NameError} je iznimka koja opisuje grešku pri pozivu \textit{imena} \texttt{'c'}.

Sintaksne greške testiraju svaku liniju koda netom prije izvođenja cijelog programa i utvrđuju svaku nepravilnost u sintaksi programskog jezika. Npr.

\begin{lstlisting}[language=Python, style=File]
j=1
   
if j<2:
    print 'Help'
else:
    if True
        print 'Help2'
\end{lstlisting}

\begin{lstlisting}[language=Python, style=Terminal]
  File "/home/mcavrak/.spyder2/.temp.py", line 7
    if True
          ^
SyntaxError: invalid syntax
\end{lstlisting}

prilikom izvođenja ovog koda program nikada niti neće ući u \textit{else} jer je uvjet \textit{if} naredbe takav da vraća \textit{True}, međutim program svejedno vraća grešku prilikom provere sintakse (\texttt{'nedostaje dvotočka iza True'})

No, iznimke će program objaviti samo ako naiđe na njih.

Primjer kada naiđe na grešku (pogrešno upisana varijabla 'i', trebalo je 'j'):

\begin{lstlisting}[language=Python, style=File]
j=1
   
if j<2:
    print 'Help'
    print i
\end{lstlisting}

\begin{lstlisting}[language=Python, style=Terminal]
Help
Traceback (most recent call last):
  File "/home/mcavrak/.spyder2/.temp.py", line 6, in <module>
    print i
NameError: name 'i' is not defined
\end{lstlisting}

Primjer kada ne naiđe na grešku:

\begin{lstlisting}[language=Python, style=File]
j=1
   
if j<2:
    print 'Help'
else:
    print i
\end{lstlisting}

\begin{lstlisting}[language=Python, style=Terminal]
Help
\end{lstlisting}

Najčešće iznimke koje susrećemo su:

\begin{itemize}
 \item \textbf{ZeroDivisionError}

pokušaj operacije djeljenja sa nulom
 
 \begin{lstlisting}[language=Python, style=Terminal]
>>> 10 * (1/0)
Traceback (most recent call last):
  File "<stdin>", line 1, in ?
ZeroDivisionError: integer division or modulo by zero
\end{lstlisting}

\item \textbf{NameError}

pokušaj operacije sa varijablom čije ime (\textit{name}) nije u popisu imena (\textit{namespace})

 \begin{lstlisting}[language=Python, style=Terminal]
>>> 4 + i*3
Traceback (most recent call last):
  File "<stdin>", line 1, in ?
NameError: name 'i' is not defined
\end{lstlisting}

\item \textbf{TypeError}

Pokušaj izvršavanja operacije na krivom tipu podataka 

\begin{lstlisting}[language=Python, style=Terminal]
>>> '2' + 2
Traceback (most recent call last):
  File "<stdin>", line 1, in ?
TypeError: cannot concatenate 'str' and 'int' objects
\end{lstlisting}

\end{itemize}

Postoji još mnogo iznimaka te ih je moguće pronaći na \url{http://docs.python.org/2/library/exceptions.html#bltin-exceptions}.

No, iznimkama možemo manipulirati, tj. moguće je ugraditi u kod identifikaciju greške te djelovanje sukladno tome. O tome više u slijedećem poglavlju.

\section{Manipulacija greškama - \textit{Exception handling}} % Chapter title
\label{sec:manipulacija_greskama} 
\index{error}
\index{exception}

Iznimkama možemo manipulirati, tj. možemo prihvatiti informaciju o greški te djelovati u skladu sa njom ili granati kod tako da nastavi sa izvršavanjem unatoč greški. Razmotrimo primjer kada korisnik unosi brojeve na konzoli pomoću \textit{raw\_input} naredbe.

\begin{lstlisting}[language=Python, style=File]
var=raw_input('Unesi broj:')
a=int(var)
print a
\end{lstlisting}

Python vraća iznimku \textbf{ValueError} jer nije u stanju pretvoriti preuzeti string u cjelobrojnu vrijednost.

\begin{lstlisting}[language=Python, style=Terminal]
Unesi broj:z
Traceback (most recent call last):
  File "/home/flood/untitled0.py", line 9, in <module>
    a=int(var)
ValueError: invalid literal for int() with base 10: 'z'
\end{lstlisting}

Kako bi izbjegli takvu grešku i prihvatili iznimku poslužiti ćemo se sa \textit{try - except} formulacijom:
\begin{lstlisting}[language=Python, style=File]
var = raw_input('Unesi broj:')

try:
    a = int(var)
    print 'SUCCESS'
except ValueError:
    print 'Nije unešen broj!'
    print 'Broj postavljen na defaultnu vrijednost 0.'
    a = 0

print a
\end{lstlisting}

U ovom kodu izvršen je prihvat iznimke na način da je:
\begin{itemize}
 \item unešena ključna riječ \textit{try} čije se tijelo prvo izvršava
 \item ako program ne naiđe na iznimku tada program preskače dio tijela koji pripada \textit{except} sekciji i nastavlja sa kodom (\texttt{'print a'}).
 \item ako program naiđe na iznimku u \textit{try} sekciji koda tada se zaustavlja na mjestu gdje je našao grešku, ne nastavlja dalje sa preostalim djelom koda (\texttt{'print SUCCESS'}) već skače na \textit{except} sekciju koja odgovara iznimki koju je uhvatio (u ovom slučaju \textbf{ValueError}) te potom nastavlja sa kodom (\texttt{'print a'})
 \item ukoliko program ne naiđe na vrstu iznimke opisanu u except sekciji tada izbaci grešku u obliku \textit{neuhvaćene iznimke} (eng. \textit{unhandled exception}) 
\end{itemize}


Pored ovog osnovnog koncepta primjene prihvata iznimaka, moguće je podići iznimku prema korisničkoj želji te izraditi korisnički definirane iznimke. Ove funkcionalnosti nadilaze osnove i čitatelj je slobodan potražiti više informacija na \url{http://docs.python.org/2/tutorial/errors.html}.


%----------------------------------------------------------------------------------------
\section{Ispravljanje grešaka - \textit{DeBugging}} % Chapter title
\label{sec:ispravljanje_gresaka} 
\index{pdb}
\index{debug}
Python ima modul \texttt{pdb} (\glqq Python DeBugger") koji pomaže kod otkrivanja i ispravljanja grešaka u kodu skripte. 
Ako ne koristite neki specijalizirani editor unutar nekog razvojnog okruženja tada je potrebno pozvati \texttt{pdb} modul na početku skripte:
\begin{lstlisting}[language=Python, style=File]
import pdb
\end{lstlisting}
Na nekom ključnom mjestu u kodu na kojem se želite zaustaviti morate staviti naredbu:
\begin{lstlisting}[language=Python, style=File]
pdb.set_trace()
\end{lstlisting}
na primjeru \ref{lst:dbg} može se vidjeti upotreba \texttt{pdb} modula.

Ako koristite editor Spyder tada nije nužno pozvati \texttt{pdb} modul. Dovoljno je postaviti prekide u kodu dvostrukim lijevim klikom 
na lijevoj strani dokumenta i pokrenuti skriptu u Debug načinu rada.


% Izvršavanje koda će se zaustaviti na mjestu koje ste označili ulazite u \texttt{pdb} modul, a u konzoli će se pojaviti tekst:
\begin{lstlisting}[language=Python, style=Terminal]
(Pdb)
\end{lstlisting}
Unutar \texttt{pdb} modula moguće je kontrolirati izvršavanje koda, a najčešće se koriste naredbe:
\begin{itemize}
 \item \texttt{n} (\emph{next}) izvršava se sljedeća naredba u skripti,
 \item  \texttt{c} (\emph{continue}) izvrešava se ostatak skripte do sljedećeg prekida,
 \item  \texttt{s} (\emph{step into}) \texttt{pdb} ulazi u podprogram ili funkciju,
 \item  \texttt{r} (\emph{return}) slično kao naredba \texttt{c}, ali \texttt{pdb} izvršava ostatak podprograma ili funkcije do izlaza iz podprograma,
 \item  \texttt{p} \texttt{var} (\emph{print}) \texttt{pdb} istpis stanja varijable \texttt{var}
 \item  \texttt{ENTER} \texttt{pdb} izvršava zadnju naredbu koja mu je poslana,
 \item  \texttt{q} (\emph{quit}) zaustavlja izvršavanje skripte. 
\end{itemize}

\lstinputlisting[language=Python, style=File, 
caption={Upotreba \texttt{dbg} modula},label=lst:dbg]{gfx/PythonLjuskaISkripte/debug.py}

