print 'Ispisi ovo na ekranu!'
print 'Ispisi' + ' jos' + ' ovo.'

a=12
type(a)
print a
print 'a=', a
print 'a=' + str(12)
print 'Ovo je a:%d' % (a)
print 'Ovo je a:%.2d' % (a)

b=15.365982
type(b)
print 'Ovo je b:',b
print 'Ovo je b:'+str(b)
print 'Ovo je b:%f' % (b)
print 'Ovo je b:%.2f' % (b)
print 'Ovo je b:%10.3f' % (b)

print 'ovo je a: %d a ovo je b: %f' % (a,b)

print 'a = {broj_a}, b = {broj_b}'.format(broj_a = a, broj_b = b)
print 'a = {broj_a}, b = {broj_b:.2f}'.format(broj_a = a, broj_b = b)
print 'a = {broj_a:8d}, b = {broj_b:8.2f}'.format(broj_a = a, broj_b = b)
