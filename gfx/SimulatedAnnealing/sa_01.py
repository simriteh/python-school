# -*- coding: utf-8 -*-
"""
Simulirano kaljenje
"""

from random import Random
from time import time
import inspyred
from numpy import *
from matplotlib.pyplot import *

F=[]

def f(X):
    fit = 10.0 * len(X)
    for x in X:
        fit += (x**2.0 - 10.0*cos(2.0*pi*x))
    return float(fit)
        
def generate(random, args):
    size = args.get('num_inputs', 10)
    return [random.uniform(-5.12, 5.12) for i in range(size)]

def evaluate(candidates, args):
    fitness = []
    for cs in candidates:
        fitness.append(f(cs))
    return fitness

def observe(population, num_generations, num_evaluations, args):
    best = min(population)
    #print '%10d > %.4e' % (num_generations, best.fitness)
    F.append(best.fitness)

prng = Random()
prng.seed(time()) 

ea = inspyred.ec.SA(prng)
ea.terminator = inspyred.ec.terminators.evaluation_termination
ea.observer = observe
final_pop = ea.evolve(evaluator = evaluate, 
                      generator = generate, 
                      maximize = False,
                      bounder = inspyred.ec.Bounder(-5.12, 5.12),
                      max_evaluations = 3000,
                      temperature = 1000.0,
                      cooling_rate = 0.5)
         
best = min(final_pop)
print('Najbolje rjesenje: \n{0}'.format(str(best)))

plot(F)
xlabel('iteracije')
xlabel('f')
grid()
show()