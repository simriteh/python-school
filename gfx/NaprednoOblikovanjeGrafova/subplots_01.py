# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

fig = plt.figure(figsize=(10,8))
hr = [i+1 for i in range(4)]
gs = gridspec.GridSpec(4, 2, height_ratios=hr, width_ratios=[2, 1])


for redak in range(4):
    for stupac in range(2):
        ax = plt.subplot(gs[redak, stupac])
        ax.annotate('redak:%d, stupac:%d' % (redak, stupac), xy=(0.5, 0.5),
                    horizontalalignment='center', verticalalignment='center')
        
plt.subplots_adjust(left=0.03, right=0.97, bottom=0.03, top=0.97, hspace=0.2, wspace=0.2)
plt.show()