#!/usr/bin/python
import scipy.optimize as optimize
from numpy import cos

def f(x):
    return cos(x)**2 + 6 - x

# 0<=cos(x)**2<=1, so the root has to be near x=7
x0, S = optimize.bisect(f,6,8,full_output=True)
print x0
print S.iterations
print S.function_calls