from scipy import optimize

def fun(t):
    return t**2-2
def fprim(t):
    return 2*t
    
print optimize.newton(fun, 1., fprim,  tol=1.e-5)

 
