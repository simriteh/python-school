# -*- coding: utf-8 -*-
"""
progib ploče
rješavanje PDJ
konačne razlike
"""

from numpy import *
from scipy.linalg import *
from matplotlib.pyplot import *
from mpl_toolkits.mplot3d import Axes3D

# ulazni podaci
q = 10000.0 # [N/m^2]
d = 0.01    # [m]
L = 2.0     # [m]
E = 2.0*10**11  # modul elastičnosti
pbr = 0.3   # Poissonov broj

D = E*d**3/(12.0*(1.0-pbr**2))  # fleksna krutost

nm = 50  # broj točaka po dimenziji

h = L/(nm-1)    # udaljenost od tocke do tocke
n = (nm-2)

# izgradnja matrice sustava i vektora d.s.
Adblok = zeros([n,n]) + diag(-4*ones(n)) + \
    diag(ones(n-1),k=1) + diag(ones(n-1),k=-1)
A=empty([0,0])
for i in range(n):
    A = block_diag(A, Adblok)
A = A + diag(ones([n**2-n]),k=n) + diag(ones([n**2-n]),k=-n)
b = h**2*q/D*ones(n**2)

# rješavanje
u = solve(A,b)
z = solve(A,u*h**2)

# pakiranje rezultata
z = reshape(z,[n,n])    # ili: z = z.reshape([n,n])
Z = zeros([nm,nm])
Z[1:(nm-1),1:(nm-1)] = -z

# prikaz
pcolor(Z)
colorbar()

# 3d
l = linspace(0,L,nm)
X, Y = meshgrid(l,l)
figure().gca(projection='3d').plot_surface(X,Y,Z, \
    rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0)

show()