from numpy import *

A = array([
    [ 1.0,  3.0,  6.0],
    [-2.0,  0.0,  3.0],
    [ 8.0, -2.0,  0.0]
    ])
b = array([2.0, 6.0, 0.0])

x = linalg.solve(A, b)

print x