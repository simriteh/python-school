ljudi = ['Marko', 'Mirela', 'Luka']
nadljudi = ['Chuck','Arnold','Steven','Silvester'] 

ljudi.append('Igor')
print ljudi
nadljudi.insert(1,'Anita')
print ljudi
nadljudi.extend(ljudi)
print nadljudi

nadljudi.pop(1)
print nadljudi
nadljudi.remove('Igor')
print nadljudi
del nadljudi[4:]
print nadljudi

print ljudi.index('Marko')
ljudi.append('Marko')
print ljudi.count('Marko')

nadljudi.sort()
print nadljudi
nadljudi.reverse()
print nadljudi