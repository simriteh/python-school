# -*- coding: utf-8 -*-
"""
Rijetke matrice
"""
from numpy import array
from scipy import sparse
from scipy.sparse.linalg import spsolve
from numpy.linalg import solve
from numpy.random import rand

#indeksi pozicija razlicitih od nule
I = array([0,3,1,0,1,2]) 
J = array([0,3,1,1,0,2])
#vrijednosti u matrici na zadanim pozicijama
V = array([4,5,7,1,2,2])
#zadavanje rijetke matrice u koordinatnom obliku
A = sparse.coo_matrix((V,(I,J)),shape=(4,4))
#Da bi koristili spsolve potrebno je prebaci matricu sustava u oblik CSR (Compressed Sparse Row matrix)
A = A.tocsr()
#desna strana linearnog sustava jedanadzbi
b = rand(4)
#rjesavanje sustava preko rijetke matrice
x = spsolve(A, b)
#rjesavanje sustava sa punom matricom sustava
x_ = solve(A.todense(), b)

print 'Matrica sustava: ',A.todense()
print 'Desna strana: ', b
print 'Rjesenje: ',x
