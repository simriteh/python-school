# -*- coding: utf-8 -*-
"""
Tridijagonalna matrica sustava
"""
import numpy as np
import scipy.linalg as la

#trodijagonalna matrica, -2 na elementima glavne dijagonale, a 1 ostali elementi
A=np.ones([3,100])
for i in range(100):
    A[1,i]=-2
    
b=np.random.rand(100)

#rjesenje sustava koji ima 1 dijagonalu ispod i jednu iznad glavne dijagonale
x=la.solve_banded((1,1),A,b)


