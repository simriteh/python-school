# -*- coding: utf-8 -*-
"""
LU faktorizacija i rješavanje više slučajno generiranih sustava
"""
import numpy as np
import scipy.linalg as la

#slučajno generirani linearni sustavi
n=8
A=np.random.rand(n,n)
b1=np.random.rand(n)
b2=np.random.rand(n)
# Permutacije redaka matrice sprema se u vektor piv
# LU faktrizacija matrice A nakon permutacije redaka sprema se u matricu lu
lu,piv=la.lu_factor(A)
#Rjesenje sustava Ax=b1
x1=la.lu_solve((lu,piv),b1)
print 'x1: ',x1
#Rjesenje sustava Ax=b2
x2=la.lu_solve((lu,piv),b2)
print 'x2: ',x2
