# -*- coding: utf-8 -*-
"""
LU faktorizacija 4x4 matrice 
"""
import numpy as np
from scipy.linalg import lu

A = np.array([[4,1,2,0],[1,3,1,1],[1,0,4,1],[1,0,1,2]])
#P premutacijska matrica zamjene redaka u matrici A
# L i U su LU faktorizacija matrice A
P,L,U=lu(A)
print 'P: ',P
print 'L: ',L
print 'U: ',U
#provjera rezultata
print 'LU-PA: ',np.dot(L,U)-np.dot(P,A)