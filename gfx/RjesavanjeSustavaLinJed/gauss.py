# -*- coding: utf-8 -*-
"""
GAUSSOVA ELIMINACIJA
"""

from numpy import *
from scipy.optimize import *

A = loadtxt('A.txt')
B = loadtxt('B.txt')

n = len(B)

# Naivna eliminacija
for k in range(0,n):
    for i in range(k+1,n):
        faktor = A[i,k]/A[k,k]
        for j in range(k+1,n):
            A[i,j] = A[i,j] - faktor*A[k,j]
        B[i] = B[i] - faktor*B[k]

# Supstitucija unatrag
X = empty(shape(B))
for i in range(n-1,-1,-1):
    suma = 0
    for j in range(i+1,n):
        suma = suma + A[i,j]*X[j]
    X[i] = (B[i]-suma)/A[i,i]

print X

# Scipy rjesenje
A = loadtxt('A.txt')
B = loadtxt('B.txt')
print linalg.solve(A,B)
