# -*- coding: utf-8 -*-
"""
LU DEKOMPOZICIJA
"""

from numpy import *

A = loadtxt('A.txt')
B = loadtxt('B.txt')

n = len(B)

# LU dekompozicija

L = zeros([n,n])
U = zeros([n,n])

for k in range(0,n):
    for i in range(k,n):
        suma = 0
        for m in range(0,k):
            suma = suma + L[i,m]*U[m,k]
        L[i,k] = A[i,k] - suma
    for j in range(k+1,n):
        suma = 0
        for m in range(0,k):
            suma = suma + L[k,m]*U[m,j]
        U[k,j] = (A[k,j]-suma)/L[k,k]

for i in range(0,n):
    U[i,i] = 1

# rjesavanje sustava

C = empty(shape(B))
X = empty(shape(B))

for i in range(0,n):
    suma = 0
    for j in range(0,i):
        suma = suma + L[i,j]*C[j]
    C[i] = (B[i]-suma)/L[i,i]
    
for i in range(n-1,-1,-1):
    suma = 0
    for j in range(i+1,n):
        suma = suma + U[i,j]*X[j]
    X[i] = C[i] - suma

print X

# Scipy rjesenje
print linalg.solve(A,B)