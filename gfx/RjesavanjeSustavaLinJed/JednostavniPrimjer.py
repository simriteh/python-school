# -*- coding: utf-8 -*-
"""
Rjesavnje linearnog sustava
"""
import numpy as np
import scipy.linalg as la

A = np.array([[5,3,2],[1,2,-1],[2,3,4]])
b = np.array([1,0,-1])
#rjesenje sustava
x = la.solve(A,b)
#provjera norme reziduala rješenja sustava
print 'Pogreška: ',la.norm(np.dot(A, x)-b)