# simply
import os
os.rename('a.txt','b.kml')

# or
import shutil
shutil.move('a.txt', 'b.kml')

 
# or if you want to copy..
import shutil
shutil.copy('a.txt', 'b.kml') 
