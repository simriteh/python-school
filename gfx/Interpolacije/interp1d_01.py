from numpy.random import *
from scipy import interpolate
from pylab import *

X = linspace(0., 10., 6)
Y = array([1.6, 4., 3.4, 1.6, 1.8, 1.2])

x = linspace(0., 10., 101)

f1 = interpolate.interp1d(X, Y, kind='linear')
y1 = f1(x)

f2 = interpolate.interp1d(X, Y, kind=2)
y2 = f2(x)

f3 = interpolate.interp1d(X, Y, kind=3)
y3 = f3(x)

figure(1, figsize=[8, 3])
plot(X, Y, 'o')
plot(x, y1, label='linearna')
plot(x, y2, label='spline 2. reda')
plot(x, y3, label='spline 3. reda')
subplots_adjust(bottom=0.1,top=0.95,left=0.05,right=0.98)
legend()
show()