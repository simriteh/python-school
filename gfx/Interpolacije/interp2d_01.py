from numpy.random import *
from scipy import interpolate
from pylab import *
from mpl_toolkits.mplot3d import Axes3D

X = linspace(0., 2., 5)
Y = linspace(0., 4., 5)
Z = array([
    [1.3, 1.4, 1.1, 1.0, 0.8],
    [1.6, 1.1, 1.2, 1.3, 1.5],
    [1.2, 1.5, 1.6, 1.5, 1.6],
    [1.2, 1.4, 1.5, 1.4, 1.3],
    [1.3, 1.2, 1.1, 1.1, 1.2]
    ])

XX, YY = np.meshgrid(X, Y)

f1 = interpolate.interp2d(X, Y, Z, kind='linear')
f2 = interpolate.interp2d(X, Y, Z, kind='cubic')

x = linspace(0., 2., 51)
y = linspace(0., 4., 31)
xx, yy = meshgrid(x, y)

z1 = f1(x, y)
z2 = f2(x, y)

fig = plt.figure(figsize=[8, 3])

subplot(1, 3, 1)
scatter(XX, YY, c=Z, marker = 's', s = 100, cmap = cm.Purples)
xlim(0, 2)
ylim(0, 4)

subplot(1, 3, 2)
CS = plt.contourf(xx, yy, z1, cmap = cm.Purples)
xlim(0, 2)
ylim(0, 4)

subplot(1, 3, 3)
CS = plt.contourf(xx, yy, z2, cmap = cm.Purples)
xlim(0, 2)
ylim(0, 4)

subplots_adjust(bottom=0.3,top=0.95,left=0.05,right=0.95)
cax = fig.add_axes([0.05, 0.1, 0.9, 0.1])
fig.colorbar(CS, cax=cax, orientation='horizontal')

show()