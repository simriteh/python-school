#!/usr/bin/python
# -*- coding: utf-8 -*-

X = [12.3, 18.6, 2.1, 12.3]

print 'Iteriranje pomocu enumerate:'
for x in enumerate(X):
    print x
    
print 'Kreni od 0:'  
for i, x in enumerate(X):
    print 'a[%d]: %f' % (i, x)

print 'Kreni od 1:'    
for i, x in enumerate(X, 1):
    print 'a[%d]: %f' % (i, x)