#!/usr/bin/python
# -*- coding: utf-8 -*-

imena = ['Vlatko', 'Ivana', 'Darko', 'Igor']
prezimena = ['Hoorvat', 'Bali', 'Copljar', 'Worry']
visine = [1.79, 1.86, 2.10, 2.03]

for i, p, v in zip(imena, prezimena, visine):
    print '%s %s (%.2f m)' % (i, p, v)