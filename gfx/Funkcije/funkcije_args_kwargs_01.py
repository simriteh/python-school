#!/usr/bin/python
# -*- coding: utf-8 -*-

def funkcija(*args, **kwargs):
    print 'non-keyword argumenti:', args
    print 'keyword argumenti:', kwargs
  
    for key, value in kwargs.iteritems():
        print "%s = %s" % (key, value)
  
funkcija(10, 'bla bla', a=3, b=True, c=(0,0))