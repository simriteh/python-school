#!/usr/bin/python
# -*- coding: utf-8 -*-

def funkcija(a, b, c, d, e):
    print 'argumenti:'
    print ' a:', a
    print ' b:', b
    print ' c:', c
    print ' d:', d
    print ' e:', e

a = (1, 2, 3, 4, 5)  
funkcija(*a)

kwa = {'d':1, 'c':2, 'a':3, 'e':4, 'b':5}  
funkcija(**kwa)

a = (5, 4)
kwa = {'e':1, 'd':2, 'c':3}
funkcija(*a, **kwa)