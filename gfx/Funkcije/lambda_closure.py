def poruka(ime):
    return lambda poruka: ime + ': ' + poruka

p = poruka('Mario')
print p('Bok!')
print p('Kako ste?')