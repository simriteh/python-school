#!/usr/bin/python

def f(a, b=0.0):
    if(a > b):
        return a - b
    else:
        return b

a = f(6.0, 2.0)
b = f(20.0)
c = f(-3.0)

print a, b, c
