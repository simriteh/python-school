#!/usr/bin/python

def f(a, b):
    z = a + b
    r = a - b
    return z, r

x = f(6.0, 2.0)
print x

c, d = f(6.0, 2.0)
print c
print d