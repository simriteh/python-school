# -*- coding: utf-8 -*-
"""
Nelder-Mead metoda
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

fig = plt.figure()
linija, = plt.plot([], [], '-o')
plt.xlim([0, 10])
plt.ylim([-1, 1])


def init():
    linija, = plt.plot([], [], '-o')
    
    return linija

def draw_frame(it):
    
    xline = np.linspace(0.0,10.0,50)
    yline = np.sin(xline + it/10.0)/(1.0 +xline**2.0)
    linija.set_data(xline, yline)
    
    return linija
    
Writer = animation.writers['ffmpeg']
writer = Writer(fps=5, metadata=dict(artist='sim.riteh.hr'), bitrate=3600)

anim = animation.FuncAnimation(fig, draw_frame, init_func=init,
   frames=500, interval=2, repeat=False)

anim.save('nelder-mead.avi', writer=writer)
plt.show()
