# -*- coding: utf-8 -*-
"""
Linearna regresija
"""

from pylab import *

X = [1.0, 0.6, 1.065, 1.5, 1.72, 2.06, 2.84, 3.1, 3.15, 3.26, 3.55, 3.61, 4.0]
Y = [10.0, 10.6, 11.5, 10.8, 11.0, 20.6, 20.8, 30.1, 32.7, 33.4, 38.0, 37.0, 48.2]

k1 = np.polyfit(X,Y,1)
print 'Regresijski pravac: y=%.3fx%+.3f' % (k1[0], k1[1])

k2 = np.polyfit(X,Y,2)
print 'Regresijska parabola: y=%.3fx^2%+.3fx%+.3f' % (k2[0], k2[1], k2[2])

xr = np.linspace(np.min(X), np.max(X), 100)
yr1 = xr*k1[0] + k1[1]
yr2 = xr**2*k2[0] + xr*k2[1] + k2[2]

plot(X, Y, 'ro', label=u'Točke')
plot(xr, yr1, 'b-', lw=2, label=u'Regresijski pravac')
plot(xr, yr2, 'g-', lw=2, label=u'Regresijska parabola')

legend(loc=2)
grid()
show()