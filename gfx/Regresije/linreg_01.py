# -*- coding: utf-8 -*-
"""
Linearna regresija
"""

from pylab import *

X = [1.0, 0.6, 1.065, 1.5, 1.72, 2.06, 2.84, 3.1, 3.15, 3.26, 3.55, 3.61, 4.0]
Y = [10.0, 10.6, 11.5, 10.8, 11.0, 20.6, 20.8, 30.1, 32.7, 33.4, 38.0, 37.0, 48.2]

k = np.polyfit(X,Y,1)
print 'Regresijski pravac: y=%.3fx%+.3f' % (k[0], k[1])

xr = np.linspace(np.min(X), np.max(X), 100)
yr = xr*k[0] + k[1]

plot(X, Y, 'ro', label=u'Točke')
plot(xr, yr, 'b-', lw=2, label=u'Regresijski pravac')

legend(loc=2)
grid()
show()