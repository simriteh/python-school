# -*- coding: utf-8 -*-

import numpy as np

M = np.array([[ 1,  2,  3,  4,  5],
              [ 6,  7,  8,  9, 10],
              [11, 12, 13, 14, 15],
              [16, 17, 18, 19, 20],
              [21, 22, 23, 24, 25]])
              
print M[2, 4]       # ispiši element na poziciji 2,4
poz = (3, 2)        # definiraj poziciju 3,2
print M[poz]        # ispiši element na poziciji poz

print M[1:-1,1:-1]  # ispiši dio polja bez prvih i zadnjih redaka i stupaca
print M[:,::-1]     # ispiši polje sa obrnutim redoslijedom stupaca
print M[::2]        # Ispiši svaki drugi redak polja