# -*- coding: utf-8 -*-
"""
SIMPSONOVA 1/3 FORMULA
"""

from numpy import *
from scipy.integrate import *

# Podintegralna funkcija
def f(x):
    return 20.0*sqrt(x) - 0.2*x**2 - 2.0*sin(x)

# Granice određenog integrala
a, b = 0.0, 10.0

# Broj podintervala
n = 40

h = (b-a)/n
X = linspace(a,b,n+1)
Y = f(X)

INT = 0.0
for i in range(0, n - 1, 2):
    INT = INT + (1.0/3.0)*h*(Y[i] + 4*Y[i+1] + Y[i+2])

# "Ručno" rješenje
print INT

# Scipy rjesenje
print simps(Y,X)