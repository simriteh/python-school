# -*- coding: utf-8 -*-

from numpy import *
from scipy.integrate import *

# Podintegralna funkcija
def f(x):
    return 20.0*sqrt(x) - 0.2*x**2 - 2.0*sin(x)

# Rješavanje određenog integrala na intervalu [0, 10]    
rj, gr = quad(f, 0, 10)

print rj