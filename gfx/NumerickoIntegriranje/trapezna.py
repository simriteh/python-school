# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

def f(x):
    return -25.0*x +200.0*x**2 +400.0*x**4 -500.0*x**5
    
# Granice integrala
    # granice (a,b)=(0.0,1.0)    
a, b = 0.0, 1.0

# Broj podintervala
n = 10

# Osiguravanje ispravnih tipova varijabli
a, b, n = float(a), float(b), int (n)

# Sirina intervala
h = (b-a)/n

# Diskretizacija, racunanje x-a i y-a za svaku tocku
X = np.linspace(a,b,n+1)
Y = f(X)

# Rucno rjesavanje određenog integrala pomocu trapezne formule
I = 0.
for i in range(n):
    I += 0.5*h*(Y[i]+Y[i+1])

print 'Rjesenje 1: %f' % I

# Rjesavanje pomoću numpy.trapz funkcije
print 'Rjesenje 2: %f' %np.trapz(Y, X)    # pomocu vektora X
print 'Rjesenje 3: %f' %np.trapz(Y, dx=h) # pomocu razmaka dx

# Crtanje
plt.figure(figsize=(8,5))
Xplt = np.linspace(min(X), max(X), 1000)
plt.plot(Xplt,f(Xplt), lw=2)
plt.plot(X, Y, 'ro')
for i in range(n+1):
    plt.plot([X[i], X[i]], [0.0, Y[i]], 'k--')
plt.fill_between(X,f(X),color='DarkSeaGreen')
plt.show()