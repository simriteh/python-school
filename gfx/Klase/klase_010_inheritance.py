# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 20:07:45 2013
"""
class Road:
    def __init__(self,name,length):
        self.name=name
        self.length=length
        
    def Info(self):
        print "Road definition"
        print "Ime:\t %15s" % self.name
        print "Length:\t %15s" % self.length
        
class Highway(Road):
    def __init__(self,name,length): # override __init__ nadklase
        Road.__init__(self,name,length) # invokacija __init__ metode nadklase
        self.type='highway'
        self.toll=50 # Kn/100km
class MainRoad(Road):
    def __init__(self,name,length): # override __init__ nadklase
        Road.__init__(self,name,length) # invokacija __init__ metode nadklase
        self.type='mainroad'
class LocalRoad(Road):
    def __init__(self,name,length): # override __init__ nadklase
        Road.__init__(self,name,length) # invokacija __init__ metode nadklase
        self.type='localroad'
    
h1=Highway('A1',1000)
m1=MainRoad('E54',540)
l1=LocalRoad('S16',125)

print h1.toll