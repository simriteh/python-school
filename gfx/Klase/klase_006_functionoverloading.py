# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 13:35:55 2013
"""

from math import sqrt

class Point:
    
    def __init__(self,x,y):
        self.x=x
        self.y=y
    
    def R(self): # funkcija koja vraca radij vektor tocke, tj. udaljenost od ishodista
        return sqrt(self.x**2 + self.y**2)
    
    # Preoptereceni operatori usporedbe tocka s obzirom na udaljenost od ishodista
    def __lt__(self,other): # less then
        return self.R() < other.R()
    def __le__(self,other): # less or equal
        return self.R() <= other.R()
    def __gt__(self,other): # greather then
        return self.R() > other.R()
    def __ge__(self,other): # greater or equal
        return self.R() >= other.R()

p1=Point(2,1)
p2=Point(1,2)
p3=Point(3,0)

print p1>p2 
print p1>=p2 
print p1<=p2 
print p1<p3 
print p1==p2 
