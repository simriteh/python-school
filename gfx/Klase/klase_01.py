 #!/usr/bin/python
 # -*- coding: utf-8 -*-

from datetime import date

class Osoba:
  """
  Klasa Osoba služi za kreiranje objekata koji sadrže ... bla bla
  """
  
  ime = ''
  prezime = ''
  godinaRodjenja = 0
  
  def __init__(self, ime, prezime, godinaRodjenja):
    self.ime = ime
    self.prezime = prezime
    self.godinaRodjenja = godinaRodjenja
    
  def starost(self):
    sada = date.today().year
    old = sada - self.godinaRodjenja
    return old
  
netko = Osoba(ime = 'Ivan', prezime = 'Tudor', godinaRodjenja = 1984)
print netko.starost()