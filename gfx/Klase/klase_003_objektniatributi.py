 #!/usr/bin/python
 # -*- coding: utf-8 -*-

class Dokument:
  """
  Klasa Dokument omogućuje instanciranje objekata Dokument
  """
  def __init__(self):
      self.ime=''
      self.sadrzaj=''

d1=Dokument()
d2=Dokument()

d1.ime='Prica01'
d1.sadrzaj='Bio jednom jedan val.'

d2.ime='ListaSerija'
d2.sadrzaj='Lud, zbunjen, normalan.\n'
d2.sadrzaj+='Sila\n'