# -*- coding: utf-8 -*-

class Table:
    
    def __init__(self,a=1.0,b=1.0):
        self.a = a
        self.b = b
        self.Area = self.a * self.b # atribut povrsine stola

    def __str__(self):
        return 'Table = %4.1f x %4.1f \n Area = %6.2f' % (self.a,self.b,self.Area)
        
    def Rotate(self): # rotira stranice a i b
        tmpA=self.a
        tmpB=self.b
        
        self.a=tmpB
        self.b=tmpA
        
        print 'Stol zarotiran!'
        
    # Preoptereceni aritmetički operatori
    
    def __add__(self,other): # zbraja dva stola
        self.a = self.a + other.a # dužinu drugog pridodajemo dužini prvog
        self.Area = self.Area + other.Area # površinu drugog dodajemo povrsini prvog
        self.b = self.Area / self.a # izračunavamo novu prosječnu širinu
        print 'Stolovi spojeni!'
    def __sub__(self,other): # oduzima dva stola
        if self.Area > other.Area: 
           if self.a > other.a:
               if self.b > other.b:
                   self.Area -= other.Area # oduzima površinu drugoga
                   self.a -= other.a # oduzima dužinu drugoga
                   self.b = float(self.Area)/self.a # izračunava novu širinu
                   print 'Stolovi oduzeti!'
               else:
                   print 'Sirina b je duža kod drugog stola.'
           else:
               print 'Duzina a je duza kod drugog stola.'
        else:
            print 'Povrsina drugog je veca od prvog stola.'
            
t1=Table(10,20)
t2=Table(20,40)
print t1
print t2
t1-t2
t2-t1
t1+t2
t1.Rotate()
t1-t2
print t1
print t2

