 #!/usr/bin/python
 # -*- coding: utf-8 -*-

class Dokument:
  """
  Klasa Dokument omogucuje instanciranje objekata Dokument
  """

  DokumentList=[]  
  
  def __init__(self,name='Dokument',content=''):
      self.ime=name
      self.sadrzaj=content
      Dokument.DokumentList.append(self)

  def Info(self):
      print 'Ime dokumenta:',self.ime
      print 'Sadrzaj:\n',self.sadrzaj

d1=Dokument('Esej','Prva recenica')
d2=Dokument('Blog01')
d3=Dokument()

for d in Dokument.DokumentList:
    d.Info()   
    print '\n'

