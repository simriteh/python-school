# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 13:35:55 2013
"""

from math import sqrt

class RVector:
    
    def __init__(self,x,y):
        self.x=x # atribut x koordinata
        self.y=y # atribut y koordinata
    
    def R(self): # metoda koja vraca duljinu radij vektora, tj. udaljenost od ishodista
        return sqrt(self.x**2 + self.y**2)
    
    # Preoptereceni operatori usporedbe radij vektora s obzirom na udaljenost od ishodista
    def __lt__(self,other): # less then
        return self.R() < other.R()
    def __le__(self,other): # less or equal
        return self.R() <= other.R()
    def __gt__(self,other): # greather then
        return self.R() > other.R()
    def __ge__(self,other): # greater or equal
        return self.R() >= other.R()
    def __eq__(self,other): # greater or equal
        return self.R() == other.R()        

p1=RVector(2,1)
p2=RVector(1,2)
p3=RVector(3,0)

print p1>p2 
print p1>=p2 
print p1<=p2 
print p3<p2 
print p1==p2 
