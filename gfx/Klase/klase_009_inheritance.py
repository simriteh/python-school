# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 20:07:45 2013
"""
class Road:
    def __init__(self,name,length):
        self.name=name
        self.length=length
    def Info(self):
        print "Road definition"
        print "Ime:\t %15s" % self.name
        print "Length:\t %15s" % self.length
class Highway(Road):
    pass # Bez lokalnih definicija
class MainRoad(Road):
    pass # Bez lokalnih definicija
class LocalRoad(Road):
    pass # Bez lokalnih definicija
    
h1=Highway('A1',1000)
m1=MainRoad('E54',540)
l1=LocalRoad('S16',125)
h1.Info()
m1.Info()
l1.Info()