 #!/usr/bin/python
 # -*- coding: utf-8 -*-

class Dokument:
  """
  Klasa Dokument omogućuje instanciranje objekata Dokument
  """
  def __init__(self,name='Dokument',content=''):
      self.ime=name
      self.sadrzaj=content


d1=Dokument('Esej','Prva recenica')
d2=Dokument('Blog01')
d3=Dokument()
