# -*- coding: utf-8 -*-

from pulp import *

# Varijable
x11 = LpVariable("x11", 0)
x12 = LpVariable("x12", 0)
x13 = LpVariable("x13", 0)

x21 = LpVariable("x21", 0)
x22 = LpVariable("x22", 0)
x23 = LpVariable("x23", 0)

x31 = LpVariable("x31", 0)
x32 = LpVariable("x32", 0)
x33 = LpVariable("x33", 0)

x41 = LpVariable("x41", 0)
x42 = LpVariable("x42", 0)
x43 = LpVariable("x43", 0)

# LP problem
prob = LpProblem("Plane Cargo", LpMaximize)

# Cilj
prob += (x11 + x12 + x13)*310 + (x21 + x22 + x23)*380 + \
        (x31 + x32 + x33)*350 + (x41 + x42 + x43)*285

# Ogranicenja
prob += x11 + x12 + x13 <= 18
prob += x21 + x22 + x23 <= 15
prob += x31 + x32 + x33 <= 23
prob += x41 + x42 + x43 <= 12

prob += x11 + x21 + x31 + x41 <= 10
prob += x12 + x22 + x32 + x42 <= 16
prob += x13 + x23 + x33 + x43 <= 8

prob += 4.80*x11 + 6.50*x21 + 5.80*x31 + 3.90*x41 <= 68
prob += 4.80*x12 + 6.50*x22 + 5.80*x32 + 3.90*x42 <= 87
prob += 4.80*x13 + 6.50*x23 + 5.80*x33 + 3.90*x43 <= 53

prob += (x11 + x21 + x31 + x41)/10 == (x12 + x22 + x32 + x42)/16
prob += (x12 + x22 + x32 + x42)/16 == (x13 + x23 + x33 + x43)/8

# Rjesavanje
status = prob.solve()

# Ispis rjesenja
print 'Status:', LpStatus[status]

for v in prob.variables():
    print v.name, "=", v.varValue

print "Total profit = ", value(prob.objective)
