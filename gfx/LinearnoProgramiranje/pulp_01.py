# -*- coding: utf-8 -*-

from pulp import *

# Varijable
x = LpVariable("x", 0, 3)
y = LpVariable("y", 0, 1)

# LP problem
prob = LpProblem("myProblem", LpMinimize)

# Cilj
prob += -4*x + y

# Ogranicenja
prob += x + y <= 2
prob += x == y 

# Rjesavanje
status = prob.solve()

# Ispis
print LpStatus[status]
print value(x)
print value(y)
