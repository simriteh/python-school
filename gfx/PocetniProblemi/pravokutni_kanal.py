# -*- coding: utf-8 -*-
"""
pravokutni kanal
transkritično strujanje
rješavanje ODJ
"""

from numpy import *
from scipy.optimize import *
from scipy.integrate import *
from matplotlib.pyplot import *

g = 9.81

# ulazni podaci o kanalu
B = 15.0
S0 = 0.001
n = 0.012

Q = 10.0

yc = ((Q/B)**2/g)**(1.0/3.0)
print('yc = '+str(yc))

# osnovne veličine
A = lambda y: B*y
P = lambda y: B+2*y
R = lambda y: A(y)/P(y)
Sf = lambda y: (n*Q/(R(y)**(2.0/3.0)*A(y)))**2

yn = fsolve((lambda y: Sf(y)-S0),yc)
print('yn = '+str(yn[0]))

# računanje razine vode
x0 = 0.0
y0 = 0.42
x1 = 30.0

X = linspace(x0,x1,100)

dydx = lambda y,x: (S0-Sf(y))/(1.0-B*Q**2/(A(y)**3*g))
Y = odeint(dydx,y0,X)

#dydx_2 = lambda y,x: S0*(1.0-(yn/y)**(10.0/3.0))/(1.0-(yc/y)**3)
#Y_2 = odeint(dydx_2,y0,X)

#crtanje
figure(1, [8, 3])
Ydno = -(X-x0)*S0
plot(X,Y[:,0]+Ydno)
plot(X,Ydno)
#plot(X,Y_2[:,0]-(X-x0)*S0);
legend(('razina vode','dno'),loc='best');
subplots_adjust(bottom=0.08,top=0.95,left=0.05,right=0.98)
show()