from pylab import *
import scikits.bvp1lg as bvp

# Definiranje problema
EI = 1.0 # Youngov modul i moment otpora
L = 1.0 # Duzina grede
# Opterecenja
qq=-1.0 

# Kontinuirana sila, poprecna sila i moment savijanja
def q(x): return qq

degrees = [1, 1, 1, 1]

# Sustav diferencijalnih jednadzbi 
def function(x , y):
    u, du, d2u, d3u = y
    return ([ du,
              d2u,
              d3u,
              q(x)/EI])

# Rubni uvjeti
boundary_points = [0.0, 0.0, L, L]
def boundary_conditions(y):
    u, du, d2u, d3u = y
    return [u[0], du[1], u[2], d2u[3]]

tol = [1e-15, 1e-15, 1e-15, 1e-15]

# Rjesavanje rubnog problema
solution = bvp.colnew.solve(
    boundary_points, degrees, function, boundary_conditions,
    is_linear=False, tolerances=tol,
    vectorized=False, maximum_mesh_size=10000)
    
# Vizualizacija rjesenja
figure(1, figsize=(8, 3))
x = solution.mesh
plot(x, solution(x)[:,0], 'b-', linewidth=2)
xlabel('x')
ylabel('y')
grid()
subplots_adjust(bottom=0.15,top=0.95,left=0.13,right=0.98)
savefig('greda.pdf')
show()