#!/usr/bin/python

# importiraj sys modula
import sys
print sys.platform    # ispisi platformu (operaticni sustav)
print sys.version     # ispisi verziju Pythona 

# importiraj modul math kao m
import math as m
print m.pi            # ispisi vrijednost broja pi
print m.sin(m.pi/2.0) # ispisi vrijednost sinusa kuta pi/2

# iz modula datetime importiraj objekt date
from datetime import date
print date.today()    # ispisi danasnji datum

# iz modula random importiraj funkciju random kao nasumicni
from random import random as nasumicni
print nasumicni()     # ispisi nasumicni broj

# Importiraj sve objekte iz modula string
from string import *
print uppercase # ispis svih velikih slova
print lowercase # ispis svih velikih slova
print digits    # ispis svih znamenaka
