# -*- coding: utf-8 -*-
"""
Nelder-Mead metoda
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

def f(x):
    return np.sin(x[0]/20.0)/(x[0]**2.0/1000.0 + 1)*np.cos(x[1]/50.0)
    #return x[0]**2.0 + x[1]**2.0


X,Y = np.meshgrid(np.arange(-60,60,2), np.arange(-60,60,2))
Z = np.sin(X/20.0)/(X**2.0/1000.0 + 1)*np.cos(Y/50.0)

fig = plt.figure()
contour_plot = plt.contourf(X, Y, Z, 150, # [-1, -0.1, 0, 0.1],
                            #alpha=0.5,
                            #cmap=plt.cm.bone,
                            #origin=origin
                            )


# Postavke metode
alpha = 1.0
beta = 0.5
gamma = 2.0
delta = 0.5
maxiter = 15
eps = 0.0001

# Početne točke
A0 = [-50, 50]
B0 = [-50, -50]
C0 = [50, 50]

A0.append(f(A0))
B0.append(f(B0))
C0.append(f(C0))

# Formiranje trokuta pomoću matrice
T = [A0, B0, C0]
xline = [A0[0], B0[0], C0[0], A0[0]]
yline = [A0[1], B0[1], C0[1], A0[1]]
simpleks, = plt.plot(xline, yline, 'k-o')

def init():
    xline = [A0[0], B0[0], C0[0], A0[0]]
    yline = [A0[1], B0[1], C0[1], A0[1]]
    simpleks, = plt.plot(xline, yline, '-o')
    
    return simpleks
    
# Glavna petlja metode
def iteration(it):
    Ts = sorted(T, key=lambda point: point[2])
    
    B = np.array(Ts[0])
    S = np.array(Ts[1])
    W = np.array(Ts[2])
    
    M = (B + S) / 2.0
    
    # Refleksija
    R = M + alpha * (M - W)
    R[2] = f(R)
    
    if R[2] >= B[2] and R[2] < S[2]:
        W = np.copy(R)
        
    elif R[2] < B[2]:
        # Ekspanzija
        E = M + gamma * (R - M)
        E[2] = f(E)
        
        if E[2] < R[2]:
            W = np.copy(E)
        else:
            W = np.copy(R)
            
    elif R[2] >= S[2]:
        
        if R[2]< W[2]:
            # Kontrakcija vanjska
            C = M + beta*(W-M)
            C[2] = f(C)
            
            if C[2] <= R[2]:
                W = np.copy(C)
            else:
                # Shrink
                W = B + delta * (W - B)
                W[2] = f(W)
                
                S = B + delta * (S - B)
                S[2] = f(S)

        else:
            # Kontrakcija unutarnja
            C = M - beta * (W - M)
            C[2] = f(C)

            if C[2] < W[2]:
                W = np.copy(C)
            else:
                # Shrink
                W = B + delta * (W - B)
                W[2] = f(W)
                
                S = B + delta * (S - B)
                S[2] = f(S)
                
    T[0] = B
    T[1] = S
    T[2] = W
    
    xline = [B[0], S[0], W[0], B[0]]
    yline = [B[1], S[1], W[1], B[1]]
    simpleks.set_data(xline, yline)
    
    #plt.savefig('iteration%02d.pdf' % it)
    return simpleks
    
Writer = animation.writers['ffmpeg']
writer = Writer(fps=2, metadata=dict(artist='Stefan'), bitrate=3600)

anim = animation.FuncAnimation(fig, iteration, #init_func=init,
   frames=maxiter, interval=2, repeat=False)

anim.save('nelder-mead.avi', writer=writer)
plt.show()

Ts = sorted(T, key=lambda point: point[2])
print Ts[0]
print Ts[1]
print Ts[2]
