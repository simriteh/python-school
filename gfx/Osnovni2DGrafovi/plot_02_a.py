from pylab import *
from numpy import *

X = np.linspace(-np.pi, np.pi, 256,endpoint=True)
C,S = np.cos(X), np.sin(X)
X2=[-3, -2, -0.5, 1.2, 2.8]
Y2=[0.3, 0.9, 0.5, 0.1, 0.62]
plot(X, C, color="blue", linewidth=2.5, linestyle="-")
plot(X, S, color="red",  linewidth=2.5, linestyle="-")
plot(X2, Y2, color="green", linestyle="-", marker="o", markersize=10)
show()
