from pylab import *
from numpy import *

x = linspace(0,10,100)
y1 = sin(x)
y2 = cos(x)

plot(x, y1, c = '0.9')
plot(x, y2, c = (0.1, 0.5, 0.7))

show() 
