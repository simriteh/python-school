from pylab import *
from numpy import *

fig = figure(figsize=[8,8])
ax = fig.add_subplot(111, polar=True)

subplots_adjust(bottom=0.1,top=0.9,left=0.1,right=0.9)

r = arange(0,1,0.001)
theta = 2*2*pi*r

ax.plot(theta,r, lw=2)
grid(True)
show() 
