from pylab import *
from numpy import *

x = linspace(0, 10, 100)
y1 = sin(x)
y2 = cos(x)

figure(1, [8, 2])
plot(x, y1, label='sinus')
plot(x, y2, label='kosinus')
legend(loc='upper center') # Postavlja legendu gore na sredinu
show() 
