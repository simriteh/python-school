from pylab import *
from numpy import *
def myticks():
    xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi],
       [r'$-\pi$', r'$-\pi/2$', r'$0$', r'$+\pi/2$', r'$+\pi$'])
    yticks([-1, 0, +1],
       [r'$-1$', r'$0$', r'$+1$'])

X = np.linspace(-np.pi, np.pi, 256,endpoint=True)
C,S = np.cos(X), np.sin(X)

figure(1)
subplot(211)
plot(X, C, color="blue", linewidth=2.5, linestyle="-")
myticks()
subplot(212)
plot(X, S, color="red",  linewidth=2.5, linestyle="-")
myticks()

show()
