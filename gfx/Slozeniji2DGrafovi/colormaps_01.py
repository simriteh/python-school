# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 18:01:52 2014

@author: stefan
"""

from pylab import *
from numpy import outer
rc('text', usetex=False)
a=outer(ones(10),arange(0,1,0.01))
maps=[m for m in cm.datad if not m.endswith("_r")]
maps.sort()
l=len(maps)+1
l=l/2

figure(figsize=(10,10))
subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=0.85, wspace=0.4, hspace=0.1)
#fig, axes = subplots(nrows=l, ncols=2)
#fig.tight_layout() # Or equivalently,  "plt.tight_layout()"

"""
for i in range(len(maps)):
    m = maps[i]
    subplot(l,1,i+1)
    axis("off")
    imshow(a,aspect='auto',cmap=get_cmap(m),origin="lower")
    title(m,rotation=0,fontsize=10, x=1.0, y=0.5,horizontalalignment='left')
"""    
for i in range(0,l*2-2,2):
    print i    
    
    m = maps[i]
    subplot(l-1,2,i+1)
    #subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=0.8)
    axis("off")
    imshow(a,aspect='auto',cmap=get_cmap(m),origin="lower")
    title(m,rotation=0,fontsize=10, x=1.05, y=0.,horizontalalignment='left')
    
    m = maps[i+1]
    subplot(l-1,2,i+2)
    #subplots_adjust(top=1.0,bottom=0.0,left=0.0,right=1.0)
    axis("off")
    imshow(a,aspect='auto',cmap=get_cmap(m),origin="lower")
    title(m,rotation=0,fontsize=10, x=1.05, y=0.,horizontalalignment='left')
    
    
#savefig("colormaps.png",dpi=100,facecolor='gray')
#tight_layout()
show()