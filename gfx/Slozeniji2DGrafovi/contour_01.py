# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as pl

# Definiranje funckije
def f(x,y):
    return (1 - np.sin(x) - np.sin(y)**3) * np.exp(-np.cos(x) -np.sin(y)**2)

# Gustoća vizualizacijskih točaka (po svakoj dimenziji)
n = 256

# Diskretizacija
x = np.linspace(-4, 0, n)
y = np.linspace(-3, 1, n)
X,Y = np.meshgrid(x, y)

# Uključivanje mreže pomoćnih linija
pl.grid()

# Crtanje površina između izolinija
# 8 - broj izolinija, alpha - prozirnost, cmap - kolormapa
Cf = pl.contourf(X, Y, f(X, Y), 8, alpha=.75, cmap=pl.cm.jet)

# Crtanje izolinija
# colors - boja linija, linewidth - debljina linija
Cl = pl.contour(X, Y, f(X, Y), 8, colors='black', linewidth=.5)

# Dodavanje oznaka na izolinije
pl.clabel(Cl, inline=1, fontsize=10)

pl.show()