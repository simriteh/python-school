# -*- coding: utf-8 -*-
"""
Created on Sat Nov 29 22:21:54 2014

@author: stefan
"""

import numpy as np
import matplotlib.pyplot as plt

# Zadavanje polinoma preko koeficijenata
p1 = np.poly1d([ 1.,  -8.3,  1.7,  5.6])
print 'p1:'
print p1

# Zadavanje polinoma preko korijena
p2 = np.poly1d([-3, -1.02, 6.75], True)
print '\np2:'
print p2

# Koeficijenti polinoma
print '\nKoeficijetni:'
print p1.c
print p2.coeffs

# Korijeni polinoma
x01 = p1.r
x02 = p2.roots
print '\nKorijeni:'
print x01
print x02

x = np.linspace(-6, 10, 200)
y1 = p1(x)
y2 = p2(x)

plt.plot(x, y1, c = 'r', label = 'p1')
plt.plot(x, y2, c = 'b', label = 'p2')
plt.plot(x01, np.zeros_like(x01), 'ro', label = 'Korijeni p1')
plt.plot(x02, np.zeros_like(x02), 'bo', label = 'Korijeni p2')
plt.grid()
plt.legend(loc = 'best')
plt.show()