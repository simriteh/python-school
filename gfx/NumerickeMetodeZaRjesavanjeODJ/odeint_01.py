# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as intgr

# ODJ dy/dx = f(y,x)
def f(y, x):
    return -y + np.sin(x)*np.cos(y)

# Početni uvjet
x0 = 0.
y0 = 20.

# Krajnja točka intervala
xf = 10

# Diskretizacija
X = np.linspace(x0, xf, 200)

# Rješavanje ODJ
Y = intgr.odeint(f, y0, X)

# Vizualizacija
plt.plot(X, Y, 'r', lw = 2)
plt.xlabel('x')
plt.ylabel('y(x)')
plt.grid()
plt.show()