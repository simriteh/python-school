# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

def f(x, y):
    return -y + np.sin(x)*np.cos(y)

x0 = 0.
y0 = 20.

xf = 15.

n = 100
dx = (xf - x0)/n

X = np.linspace(x0, xf, n + 1)
Y = [y0]

for i in range(n):
    y = Y[i] + dx*f(X[i], Y[i])
    print f(X[i], Y[i]), y
    Y.append(y)

print Y
    
plt.plot(X, Y)
plt.xlabel('x')
plt.ylabel('y(x)')
plt.title(r"$\frac{dy}{dx} = -y + sin(x)cos(y)$")
plt.show()