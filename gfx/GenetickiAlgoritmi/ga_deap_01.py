# -*- coding: utf-8 -*-
"""
Created on Wed Dec 18 15:15:56 2013

@author: stefan
"""

import random

from deap import base
from deap import creator
from deap import tools
from deap import algorithms
import matplotlib.pyplot as plt
import numpy as np

DIMS = 10
LB = -100.0*np.ones(DIMS)
UB = 100*np.ones(DIMS)

# Funkcija cilja
# Rosenbrock funkcija
def evaluate(x):
    x = np.array(x)
    s = sum(100.0*(x[1:]-x[:-1]**2.0)**2.0 + (1-x[:-1])**2.0)
    return s,
    
# Definicija funkcije korekcije
# Korigira gornju i donju granicu varijabli
def checkBounds(lb, ub):
    def decorator(func):
        def wrapper(*args, **kargs):
            offspring = func(*args, **kargs)
            for child in offspring:
                for i in xrange(len(child)):
                    if child[i] > ub[i]:
                        child[i] = ub[i]
                    elif child[i] < lb[i]:
                        child[i] = lb[i]
            return offspring
        return wrapper
    return decorator
    
# Kreiranje minimizacijskog problema
creator.create("FitnessMax", base.Fitness, weights=(-1.0,))
# Definicija jedinki 
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()

# Registracija optimizacijske varijable
toolbox.register("attr_float", random.uniform, -100.0, 100.0)
# Registriracija jedinke
toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_float, DIMS)
# Registracija populacije
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    
# Registracija funkcije cilja
toolbox.register("evaluate", evaluate)
# Registracija operatora križanja
toolbox.register("mate", tools.cxTwoPoints)
# Registracija korekcije poslije križanja
toolbox.decorate("mate", checkBounds(LB, UB))
# Resgistracija mutacije
toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=1, indpb=0.1)
# Registracija korekcije poslije mutacije
toolbox.decorate("mutate", checkBounds(LB, UB))
# Registracija metode selekcije
toolbox.register("select", tools.selTournament, tournsize=3)

# Kreiranje populacije jedinki
pop = toolbox.population(n=250)
# Definiraj pamćenje jedne najbolje jedinke
hof = tools.HallOfFame(1)

# Statističko praćenje algoritma
stats = tools.Statistics(lambda ind: ind.fitness.values)
stats.register("avg", tools.mean)
stats.register("std", tools.std)
stats.register("min", min)
stats.register("max", max)

# Pokretanje algoritma
algorithms.eaSimple(pop, toolbox, cxpb=0.75, mutpb=0.8, ngen=200, stats=stats,
                    halloffame=hof, verbose=True)

# Ispis najbolje jedinke
print hof

# Crtanje grafa konvergencije
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.set_yscale('log')
plt.plot(stats.data['min'][0], '-g', label = 'min')
plt.plot(stats.data['avg'][0], '-b', label = 'avg')
plt.plot(stats.data['max'][0], '-r', label = 'max')
plt.grid()
plt.legend()
plt.show()