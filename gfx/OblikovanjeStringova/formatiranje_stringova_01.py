from math import sqrt, sin

l = range(1, 11) 

for i in range(len(l)):
    s = '%03d %4.1f %6.1f %5.2f %+6.3f' % \
        (i+1, l[i], l[i]**2.0, sqrt(l[i]), sin(l[i]))
    print s