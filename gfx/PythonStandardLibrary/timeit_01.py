#!/usr/bin/python
# -*- coding: utf-8 -*-

import timeit
from math import sqrt

# Algoritam
def f(x):
    s = 0.0
    for i in range(x):
        s = s * sqrt(s+x)
        
t = timeit.Timer(stmt='f(500)', setup='from __main__ import f')

r1 = t.timeit(number=1000)
print 'Potrebno vrijeme za 1000 poziva funkcije f(500): %.5f' % r1

r5 = t.repeat(number=1000, repeat=5)
print '\nPonavljanje testa (5 puta):'
for r in r5:
    print 'Potrebno vrijeme za 1000 poziva funkcije f(500): %.5f' % r