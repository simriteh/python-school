#!/usr/bin/python
# -*- coding: utf-8 -*-

import time

# dohvati vrijeme početka
start_c = time.clock()
start_t = time.time()
print 'Start clock: %.5f' % start_c
print 'Start time: %.5f' % start_t


# Algoritam
a = 0.0
for i in range(1000):
    for j in range(i):
        a = a + (i - j)**0.3
        
# dohvati vrijeme završetka
stop_c = time.clock()
stop_t = time.time()
print 'Stop clock: %.5f' % stop_c
print 'Stop time: %.5f' % stop_t

print 'Rezultat: %.2f' % a  
print 'Vrijeme izvrsavanja (clock): %.3f ms' % (stop_c - start_c)
print 'Vrijeme izvrsavanja (time): %.3f ms' % (stop_t - start_t)