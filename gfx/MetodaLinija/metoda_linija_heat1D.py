#!/usr/bin/python
# -*- coding: utf-8 -*-

from scipy import *
from scipy.integrate import *
from matplotlib.pyplot import *
from matplotlib.animation import *

nx = 50
x = linspace(0, 1, nx)
A = diag(ones(nx)*(-2.0), k=0) + \
    diag(ones(nx-1)*1.0, k=1) + \
    diag(ones(nx-1)*1.0, k=-1) 

"""
Resetiraj jednadžbu za prvu i zadnju točku
"""
A[0,:] = 0
A[-1,:] = 0

"""
Prva točka: izolacija dT0/dx = 0 ==> T_-1 = T_0
dT0/dt = k*(T_1 - 2*T_0 +T_-1)/dx^2
       = k*(T_1 - T_0)/dx^2
"""
A[0,0] = -1
A[0,1] = 1

"""
Zadnja točka: fiksna temperatura dT/dt = 0
"""
A[-1,-1] = 0


dx=0.1;
k=0.01;
nt = 11
t = linspace(0.0, 100., nt)

T0 = sin(x*pi)
print shape(dot(A,T0))

"""
Postavi sustav ODJ
"""
def dT_dt(T, t):
    dT = (k/dx**2.0)*dot(A,T)
    return dT

"""
Rješi sustav ODJ
"""
rez = odeint(dT_dt, T0, t)

"""
Crtanje i animacija rješenja
"""
fig1 = figure()
lineGeom, = plot([], [], 'r-', lw=2)
titl = title('')

def update_line(i):
    lineGeom.set_data(x, rez[i,:])
    titl.set_text('t=%f' % t[i])
    #fig1.savefig('img_%03d.png' % i)
    return

anim = FuncAnimation(fig1, update_line, nt, interval=1000, repeat_delay=0)
show()