**Python u računarskom inženjerstvu**

*Numeričke metode, priprema, obrada i vizualizacija podataka*

-


Kao popratni materijal u nastavi na Tehničkom fakultetu u Rijeci a predviđeno je i za nastavu Odjela za matematiku, u izradi je skripta koja pokriva primjenu Python jezika i cijelu lepezu popratnih modula na problematiku računarskog inženjerstva, numeričke i primjenjene matematike, vizualizacije i sličnih tema.
Na izradi skripte sudjelovali su sadašnji i bivši članovi Zavoda za mehaniku fluida i računarsko inženjerstvo (http://sim.riteh.hr)

Skripta je izrađena u LaTeX-u te je besplatno dostupna našim studentima, ali i široj zainteresiranoj javnosti i objavljena je pod Creative Commons Attribution-ShareAlike 3.0 Unported License.

Skripta se kontinuirano dopunjava, ali još uvijek ima nepotpunih i nedovršenih poglavlja.
Prvi dio skripte sadrži poglavlja vezana u osnove Python jezika, te bi mogao biti od korsiti onima koji žele naučiti Python, ali ne nužno i njegovu primjenu u rješavanju inženjerskih/matematičkih problema.

PDF skipte je dostupna za download na:
https://bitbucket.org/simriteh/python-school/downloads/python-skripta.pdf

Bili bi vrlo zahvalni ako bi čitatelji/korisnici skripte dopridonijeli kvaliteti prezentiranih tema na način da greške (pravopis, gramatika, greške u formulama/kodovima i sve ostalo) na koje naiđete prijavite na https://bitbucket.org/simriteh/python-school/issues.
Osim prijave grešaka, naravno da su dobrodošli savjeti i ideje o temama koje bi bilo dobro ubaciti u skriptu!